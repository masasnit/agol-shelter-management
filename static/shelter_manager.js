//For use with the shelter manager tool.
var d1 = new $.Deferred();
function init(){
	createWebMap()
}


//Pull in layer Info
function getLayerInfo(){
	url = "/query?where=OBJECTID%3E0&geometryType=esriGeometryEnvelope&spatialRel=esriSpatialRelIntersects&units=esriSRUnit_Meter&outFields=*&returnGeometry=true&f=pjson&token="
	if(app_Settings.token != undefined){
		url = url + app_Settings.token
	}
	getAjaxCall(url, buildTable)
}

//build the shelter edit table
function buildTable(response){
	jsonData = JSON.parse(response)
	tableData = []
	for(var i = 0; i < jsonData.features.length; i++){
		tableData.push(jsonData.features[i].attributes)
		tableData[i].geometry = jsonData.features[i].geometry
		tableData[i].space = jsonData.features[i].attributes.available + "/" + jsonData.features[i].attributes.capacity
		var statusColour = ""
		if(jsonData.features[i].attributes.statuscolour == "RED"){
			statusColour = '<div class="alert alert-danger" role="alert">'
		}else if(jsonData.features[i].attributes.statuscolour == "YELLOW"){
			statusColour = '<div class="alert alert-warning" role="alert">'
		}else if(jsonData.features[i].attributes.statuscolour == "GREEN"){
			statusColour = '<div class="alert alert-success" role="alert">'
		}
	
		if(jsonData.features[i].attributes.status == "OPEN"){
			tableData[i].combinedStatus = statusColour +"Open</div>"
		}else if(jsonData.features[i].attributes.status == "NEARCAPACI"){
			tableData[i].combinedStatus = statusColour + "Near Capacity</div>"
		}else if(jsonData.features[i].attributes.status == "ATCAPACITY"){
			tableData[i].combinedStatus = statusColour + "At Capacity</div>"
		}else if(jsonData.features[i].attributes.status == "CLOSED"){
			tableData[i].combinedStatus = statusColour + "Closed</div>"
		}
		
		//only add the edit button to shelters controlled by the user
		if(jsonData.features[i].attributes.Creator == app_Settings.userId){
			tableData[i].edit = "<button onclick=editEntry(" + jsonData.features[i].attributes.OBJECTID + ") class='btn btn-primary btn-block'>EDIT</button>"
			tableData[i].masas = "<button  onclick=buildMasasEntry("  + jsonData.features[i].attributes.OBJECTID + ") class='btn btn-success btn-block'>MASAS</button>"
			//tableData[i].tweet = "<button onclick=writeTweet(" + jsonData.features[i].attributes.OBJECTID + ") class='btn btn-info btn-block disabled'>TWEET</button>"
		}
		else{
			tableData[i].edit = " "
            //tableData[i].tweet = " "
            tableData[i].masas = " "
		}
		
	}
	app_Settings.tableData = tableData
	$('#shelterTable').DataTable( {
		destroy: true,
    	data: tableData,
    	paging: false,
	    columns: [
	        { data: 'name' },
	        { data: 'description' },
	        { data: 'space' },
	        { data: 'locationName' },
	        { data: 'alternatives' },
	        { data: 'combinedStatus' },
	        { data: 'edit' },
	        { data: 'masas' }
	    ]
	} );
	//show the table once it finishes loading
	$("#shelterTable").show()
}




//build a masas Entry by pulling the info from the stored table
function buildMasasEntry(OBJECTID){
	//Disable the button until transaction is complete
	$("MASASButton" + OBJECTID ).prop('disabled', true);
	$.when( d1 ).done(function ( v1 ) {
	    $("MASASButton" + OBJECTID ).prop('disabled', false);
	});
	var masas = {}
	for(var i = 0; i < app_Settings.tableData.length; i++){
		if(app_Settings.tableData[i].OBJECTID == OBJECTID){
			masas.name = app_Settings.tableData[i].name;
			masas.description = app_Settings.tableData[i].description;
			masas.active = app_Settings.tableData[i].active;
			masas.available = app_Settings.tableData[i].available;
			masas.capacity = app_Settings.tableData[i].capacity;
			masas.status = app_Settings.tableData[i].status;
			masas.statuscolour = app_Settings.tableData[i].statuscolour;
			masas.locationName = app_Settings.tableData[i].locationName;
			masas.alternatives = app_Settings.tableData[i].alternatives;
			masas.geometry = app_Settings.tableData[i].geometry;
			masas.userId = app_Settings.userId;
			masas.masasId = app_Settings.tableData[i].masasId;
			masas.OBJECTID = OBJECTID;
			masas.link = app_Settings.tableData[i].link;
		}
	}
	var entryData = {"masas" : JSON.stringify(masas)}
	sendToPython(entryData,"masas/", masasEntrySuccess, masasEntryFail)
}

//Store masas id if created
function masasEntrySuccess(response){
	$("#alertMessages").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Masas entry sent.</div>');
	masasIdSplit = response.split(':');
    url = '/updateFeatures?f=json&token=' + app_Settings.token
	feature = [{
      "attributes" : {
      	"OBJECTID" : masasIdSplit[3],
        "masasId" : masasIdSplit[2]
      }
    }]
	sendAjaxCall(url, feature, masasEntryUpdated)
}

//report back an ajax failure
function masasEntryFail(response){
	console.log("Resolving Fail");
	d1.resolve();
	$("#alertMessages").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured while sending to Masas.</div>');
}

//Send back successful result
function masasEntryUpdated(response){
	console.log("Resolving Success");
	d1.resolve();
}


//load the edit page with the OBJECTID as a parameter
function editEntry(OBJECTID){
	window.location.replace( $(location).attr('pathname') + "edit/?OBJECTID=" + OBJECTID);
}

//Don't build the table until the token is stored.
function storeToken(token, userId){
	console.log("Storing Token: " + token)
	app_Settings.token = token
	app_Settings.userId = userId
	console.log(token)
	getLayerInfo()
	$("#userName").text("Welcome, " + userId);
}

//Check with server if user has twitter access
function writeTweet(OBJECTID){
	url = "/checkTweet"
	var tweetData = {"OBJECTID": OBJECTID}
	getFromPython(tweetData, url, checkTweetAccess, tweetFail)
}

//report back an ajax failure
function tweetFail(response){
	console.log("Tweet Fail");
	$('#tweetModal').modal('hide')
	$("#alertMessages").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured while attempting Tweet.</div>');
}

//report back on success of tweet
function tweetSuccess(response){
	$('#tweetModal').modal('hide')
	if(response.indexOf("error") == -1){
		$("#alertMessages").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Tweet sent.</div>');
	}else{
		console.log("Tweet Fail");
		$("#alertMessages").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong>' + response + '</div>');
	}
}

//Check for a success twitter login
function checkTweetAccess(response){
	console.log(response)
	jsonData = JSON.parse(response)
	if (jsonData["success"] == "True"){
		//Find the shelter in the stored information and populate the tweet
		for(var i = 0; i < app_Settings.tableData.length; i++){
			if(app_Settings.tableData[i].OBJECTID == jsonData["OBJECTID"]){
				$('#tweetOBJECTID').val(jsonData["OBJECTID"])
				$('#tweetGeom').val(app_Settings.tableData[i].geometry["x"] + "|" +app_Settings.tableData[i].geometry["y"])
				textBoxVal = app_Settings.tableData[i].name + " is " + app_Settings.tableData[i].status + ": " + app_Settings.tableData[i].available + "/" + app_Settings.tableData[i].capacity + " available " + app_Settings.tableData[i].link + " #SHELTER"
			}
		}
		$('#tweetBox').val(textBoxVal)
		$('#modalLoginInfo').text("Twitter Account: " + jsonData["screen_name"])
		
		$('#tweetModal').modal('show')
		//Warn a user if the twitter account sends geo location information
		if(jsonData["geo_enabled"] == "False"){
			$('#twitterGeoWarning').css("display", "inline")
		}else{
			$('#twitterGeoWarning').css("visibility", "none")
		}
	}else{
		window.location.replace( $(location).attr('pathname') + "authenticate");
	}
}

function sendTweet(tweetSuccess, tweetFail){
	console.log($('#tweetGeom').val())
	url = "/sendTweet"
	tweet = {
      "status" : $('#tweetBox').val(),
      "OBJECTID" : $('#tweetOBJECTID').val(),
      "geom" : $('#tweetGeom').val()
    }
    var tweetData = {"tweetData" : JSON.stringify(tweet)}
	sendToPython(tweetData, url, tweetSuccess, tweetFail)
}

function createWebMap(){
	var webmap = {};
    webmap.item = {
        "title": "Shelter Manager (by DRDC/Centre for Security Science",
        "snippet": "This map provides Shelter Management capabilities for multiple organizations sharing information in a single ArcGIS Online layer."
        //TODO: Add Extent via configuration?
    };

    webmap.itemData = {
        "baseMap": {
        "baseMapLayers": [{
          "opacity": 0.8,
          "visibility": true,
          "url":"https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"
          //"url": "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
          }],
        "title": "Topo..."
      },
      "version": "1.1"

    };

    var map;
    require([
      "esri/map",
      "esri/arcgis/utils",
      // Popup & Shelter Layer includes
      "esri/dijit/Geocoder", "esri/layers/FeatureLayer","esri/InfoTemplate","dojo/dom-style", "dojo/dom", "dojo/on", "esri/arcgis/OAuthInfo", "esri/IdentityManager","esri/graphic","esri/geometry/Point",
      "dojo/domReady!"],function(Map, arcgisUtils, Geocoder,  FeatureLayer, InfoTemplate, domStyle, dom, on, ArcGISOAuthInfo, esriId, Graphic, Point){
		
			
		  //Create the webmap
          arcgisUtils.createMap( webmap, "mapDiv").then(function (response) {
          app_Settings.map = response.map;
          //var legend = new Legend({
          //  map: map,
          //  layerInfos:(arcgisUtils.getLegendLayers(response))
          //}, "legendDiv");
          var startExtent = new esri.geometry.Extent(app_Settings.extentParams.xmin, app_Settings.extentParams.ymin, app_Settings.extentParams.xmax, app_Settings.extentParams.ymax, new esri.SpatialReference({wkid:4326}) );
		  //Set map extent based on app settings
  		  app_Settings.map.setExtent(startExtent);
  		  //create address search bar
		  geocoder = new Geocoder({ 
            map: app_Settings.map 
          }, "search");
          geocoder.startup();
          // Create the custom popup for the Shelters.
          // get the Shelters layer.
            // var shelterLayer = app_Settings.map.getLayer(1); // TODO: Get layer configured - OR - load by Name (better)
            /*
            Fields to load:
            name, description, active, capacity, available, statuscolour, locationName, status, alternatives
            // only name & description here so far...
            */
            var infoTemplate = new InfoTemplate();
				infoTemplate.setTitle("Shelter Info Window");
				infoTemplate.setContent(setInfoTemplate);
				
			function setInfoTemplate(graphic){
				console.log("Setting template")
				
				var attr = graphic.attributes;
				console.log(attr.Creator + " equals " + app_Settings.userId)
	
				templateContent = attr.name + " is " + attr.statuscolour + ": " + attr.available + "/" + attr.capacity +  " beds available.<br/>"
				if(attr.Creator == app_Settings.userId){
					templateContent += "<button onclick=editEntry(\"" + attr.OBJECTID + "\") class='btn btn-primary btn-block'>EDIT</button>"
			        templateContent += "<button onclick=writeTweet(\"" + attr.OBJECTID + "\") class='btn btn-info btn-block disabled'>TWEET</button>"
			        templateContent += "<button id=\"MASASButton" + attr.OBJECTID + "\" onclick=buildMasasEntry(\"" + attr.OBJECTID + "\") class='btn btn-success btn-block'>MASAS</button>"
				}
			                  
			    return templateContent
			}

            // Add the FeatureLayer for the Shelters...
            // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1
			
            // TODO: move URI for layer into JSON config file.
            app_Settings.shelterLayer = new FeatureLayer(app_Settings.layerURL, {
                mode: FeatureLayer.MODE_SNAPSHOT,
                infoTemplate: infoTemplate,
                outFields: ["*"]
            });
 			//set the symbol used for drawing shelters
			var defaultSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_White.png","contentType":"image/png","width":24,"height":24});
			var redSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Red.png","contentType":"image/png","width":24,"height":24});
			var greenSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Green.png","contentType":"image/png","width":24,"height":24});
			var yellowSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Yellow.png","contentType":"image/png","width":24,"height":24});
			var renderer = new esri.renderer.UniqueValueRenderer(defaultSymbol, "statuscolour");
			renderer.addValue("RED", redSymbol);
			renderer.addValue("YELLOW", yellowSymbol);
			renderer.addValue("GREEN", greenSymbol);
			app_Settings.shelterLayer.setRenderer(renderer);
			

            // add the layer
            app_Settings.map.addLayer(app_Settings.shelterLayer);
            //shelterLayer.infoTemplate = popupTemplate;
            // end - Shelter Layer & Popup

          //legend.startup();
        });
        
		console.log("setting viz for sign-in/out");

        domStyle.set("sign-in", "display", "none");
        domStyle.set("sign-out", "display", "none");
        domStyle.set("userName", "display", "none");
        // domStyle.set("createShelterButton", "display", "none");

        // OAuth2 login info:
        var info = new ArcGISOAuthInfo({
        appId: app_Settings.appId,
          // Uncomment this line to prevent the user's signed in state from being shared
          // with other apps on the same domain with the same authNamespace value.
          //authNamespace: "portal_oauth_inline",
          popup: false
        });
        esriId.registerOAuthInfos([info]);

        esriId.checkSignInStatus(info.portalUrl).then(
          function(cred) {
            domStyle.set("sign-in", "display", "none");
            domStyle.set("sign-out", "display", "inline");
            domStyle.set("userName", "display", "inline");
            // domStyle.set("createShelterButton", "display", "inline");
            // console.log(cred);
            console.log("User Signed In...");
            storeToken(cred.token, cred.userId)
			
          }
        ).otherwise(
          function() {
            // Anonymous view
            domStyle.set("sign-in", "display", "inline");
            domStyle.set("sign-out", "display", "none");
            domStyle.set("userName", "display", "none");
            // domStyle.set("createShelterButton", "display", "none");
			storeToken(undefined, undefined)
            console.log("NOT SIGNED IN...");
            //domStyle.set("anonymousPanel", "display", "block");
            //domStyle.set("personalizedPanel", "display", "none");
          }
        );
        // end OAuth2
        // hook up click handlers.
        on(dom.byId("sign-in"), "click", function() {
          console.log("click", arguments);
          // user will be redirected to OAuth Sign In page
          esriId.getCredential(info.portalUrl);

        });

        on(dom.byId("sign-out"), "click", function() {
        	domStyle.set("userName", "display", "none");
        	// domStyle.set("createShelterButton", "display", "none");
          esriId.destroyCredentials();
          window.location.reload();
        });
		
        // get the layer and create the custom popup.
        // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1

      });
}



