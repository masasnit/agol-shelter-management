//For use with the shelter manager tool.
var d1 = new $.Deferred();
var d2 = new $.Deferred();

function init(){
	createWebMap();
	$.when( d1, d2 ).done(function ( v1, v2 ) {
	    getLayerInfo()
	});
}

function getLayerInfo(){
	app_Settings.OBJECTID = getParameterByName('OBJECTID');
	url = "/query?where=OBJECTID=" + app_Settings.OBJECTID + "&outFields=*&returnGeometry=true&f=pjson&token="
	if(app_Settings.token != undefined){
		url = url + app_Settings.token
	}
	getAjaxCall(url,loadShelterEdits)
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


//load the edit fields with the information provided by ArcGIS Online
function loadShelterEdits(response){
	app_Settings.shelter = JSON.parse(response) //Parse the response
	// console.log(shelter)
	$("#viewShelterName").val(app_Settings.shelter.features[0].attributes.name);
	$("#viewShelterDescription").val(app_Settings.shelter.features[0].attributes.description);
	$("#viewShelterActive").val(app_Settings.shelter.features[0].attributes.active);
	$("#viewShelterAvailable").val(app_Settings.shelter.features[0].attributes.available);
	$("#viewShelterCapacity").val(app_Settings.shelter.features[0].attributes.capacity);
	$("#viewShelterStatus").val(app_Settings.shelter.features[0].attributes.status);
	$("#viewShelterStatusColour").val(app_Settings.shelter.features[0].attributes.statuscolour);
	$("#viewShelterLocation").val(app_Settings.shelter.features[0].attributes.locationName);
	$("#viewShelterAlternatives").val(app_Settings.shelter.features[0].attributes.alternatives);
	pt = app_Settings.shelter.features[0].geometry
	var factor = app_Settings.extentZoomFactor; 
	var extent = new esri.geometry.Extent(pt.x - factor, pt.y - factor, pt.x + factor, pt.y + factor, new esri.SpatialReference({wkid:app_Settings.shelter.spatialReference.wkid}));
	//Set map extent based on app settings
	console.log(app_Settings.map)
	app_Settings.map.setExtent(extent);
}

//load the edit page with the OBJECTID as a parameter
function editEntry(OBJECTID){
	path = $(location).attr('pathname').split("/")
	var url = "/"
	//remove the last part of the path which is the view part of the URL
	for(var i = 1; i < path.length - 2 ; i++){
		url += path[i] + "/"
	}
	window.location.replace( url + "edit/?OBJECTID=" + OBJECTID);
}


//Add buttons if you are the shelter owner
function addOwnerButtons(){
    OBJECTID = getParameterByName('OBJECTID')
	$("#buttonGroup").append('<button type="button" class="btn btn-primary" onclick=editEntry("' + OBJECTID + '")>Edit</button>')
	//$("#buttonGroup").append('<button type="button" class="btn btn-info" onclick=buildTweet("' + OBJECTID + '")>Tweet(Chatter)</button>')
	$("#buttonGroup").append('<button type="button" class="btn btn-info disabled" onclick=buildTweet("' + OBJECTID + '")>Tweet</button>')
	$("#buttonGroup").append('<button type="button" class="btn btn-success" onclick=buildMasasEntry("' + OBJECTID + '")>Masas</button>')
}

//Don't build the table until the token is stored.
function storeToken(token, userId){
	console.log("Storing Token")
	app_Settings.token = token
	app_Settings.userId = userId
	$("#userName").text("Welcome, " + userId);
}

//build a tweet/chatter message by pulling the info from the stored shelter info
function buildTweet(OBJECTID){

	alert("Twitter disabled for Exercise.");
	
// 	var Tweet = {}
// 	Tweet.name = app_Settings.shelter.features[0].attributes.name
// 	Tweet.description = app_Settings.shelter.features[0].attributes.description
// 	Tweet.active = app_Settings.shelter.features[0].attributes.active
// 	Tweet.available = app_Settings.shelter.features[0].attributes.available
// 	Tweet.capacity = app_Settings.shelter.features[0].attributes.capacity
// 	Tweet.status = app_Settings.shelter.features[0].attributes.status
// 	Tweet.statuscolour = app_Settings.shelter.features[0].attributes.statuscolour
// 	Tweet.locationName = app_Settings.shelter.features[0].attributes.locationName
// 	Tweet.alternatives = app_Settings.shelter.features[0].attributes.alternatives
// 	Tweet.geometry = app_Settings.shelter.features[0].geometry
// 	Tweet.userId = app_Settings.userId
// 	Tweet.viewURL = $(location).attr('protocol') + "//" + $(location).attr('hostname')+ $(location).attr('pathname') + "?OBJECTID=" + OBJECTID
// 	console.log(Tweet.viewURL)
// 	var entryData = {"Tweet" : JSON.stringify(Tweet)}
// 	
// 	//find the relative path to the send python URL for tweets
// 	path = $(location).attr('pathname').split("/")
// 	var url = "/"
// 	
// 	//remove the last part of the path which is the view part of the URL
// 	for(var i = 1; i < path.length - 2 ; i++){
// 		url += path[i] + "/"
// 	}
// 	
// 	url += "generator/"
// 	sendToPython(entryData,url, sendTweetSuccess, sendTweetFail)
}

//report back masas ID Success
function sendTweetSuccess(response){
	$("#alertMessages").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Chatter message sent.</div>');
}

//report back masas ID failure
function sendTweetFail(response){
	$("#alertMessages").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured while sending to Chatter.</div>');
}

//build a masas Entry by pulling the info from the stored shelter info
function buildMasasEntry(OBJECTID){
	var masas = {}
	masas.name = app_Settings.shelter.features[0].attributes.name
	masas.description = app_Settings.shelter.features[0].attributes.description
	masas.active = app_Settings.shelter.features[0].attributes.active
	masas.available = app_Settings.shelter.features[0].attributes.available
	masas.capacity = app_Settings.shelter.features[0].attributes.capacity
	masas.status = app_Settings.shelter.features[0].attributes.status
	masas.statuscolour = app_Settings.shelter.features[0].attributes.statuscolour
	masas.locationName = app_Settings.shelter.features[0].attributes.locationName
	masas.alternatives = app_Settings.shelter.features[0].attributes.alternatives
	masas.geometry = app_Settings.shelter.features[0].geometry
	masas.userId = app_Settings.userId
	masas.masasId = app_Settings.shelter.features[0].attributes.masasId
	masas.OBJECTID = OBJECTID
	masas.viewURL = $(location).attr('protocol') + "//" + $(location).attr('hostname')+ $(location).attr('pathname') + "view/?OBJECTID=" + OBJECTID
	
	//find the relative path to the send python URL for tweets
	path = $(location).attr('pathname').split("/")
	var url = "/"
	
	//remove the last part of the path which is the view part of the URL
	for(var i = 1; i < path.length - 2 ; i++){
		url += path[i] + "/"
	}
	
	url += "masas/"
	var entryData = {"masas" : JSON.stringify(masas)}
	sendToPython(entryData,url, masasEntrySuccess, masasEntryFail)
}

//Store masas id if created
function masasEntrySuccess(response){
	$("#alertMessages").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Masas entry sent.</div>');
	masasIdSplit = response.split(':');
	// console.log(masasIdSplit[2])
    url = '/updateFeatures?f=json&token=' + app_Settings.token
	feature = [{
      "attributes" : {
      	"OBJECTID" : masasIdSplit[3],
        "masasId" : masasIdSplit[2]
      }
    }]
	sendAjaxCall(url, feature)
}

//report back an ajax failure
function masasEntryFail(response){
	$("#alertMessages").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured while sending to Masas.</div>');
}

function createWebMap(){
	var webmap = {};
    webmap.item = {
        "title": "Shelter Manager (by DRDC/Centre for Security Science",
        "snippet": "This map provides Shelter Management capabilities for multiple organizations sharing information in a single ArcGIS Online layer."
        //TODO: Add Extent via configuration?
    };

    webmap.itemData = {
      "baseMap": {
      	"baseMapLayers": [{
					"opacity": 0.8,
					"visibility": true,
					"url":"https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer",
          //"url": "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
      	}],
        "title": "Topo..."
      },
      "version": "1.1"

    };

    var map;
    require([
      "esri/map",
      "esri/arcgis/utils",
      // Popup & Shelter Layer includes
      "esri/dijit/Geocoder", "esri/layers/FeatureLayer","esri/InfoTemplate","dojo/dom-style", "dojo/dom", "dojo/on", "esri/arcgis/OAuthInfo", "esri/IdentityManager","esri/graphic","esri/geometry/Point",
      "dojo/domReady!"],function(Map, arcgisUtils, Geocoder,  FeatureLayer, InfoTemplate, domStyle, dom, on, ArcGISOAuthInfo, esriId, Graphic, Point){
		
			
		  //Create the webmap
          arcgisUtils.createMap( webmap, "mapDiv").then(function (response) {
          app_Settings.map = response.map;

  		  //create address search bar
		  geocoder = new Geocoder({ 
            map: app_Settings.map 
          }, "search");
          geocoder.startup();
          // Create the custom popup for the Shelters.
          // get the Shelters layer.
            // var shelterLayer = app_Settings.map.getLayer(1); // TODO: Get layer configured - OR - load by Name (better)
            /*
            Fields to load:
            name, description, active, capacity, available, statuscolour, locationName, status, alternatives
            // only name & description here so far...
            */
            var infoTemplate = new InfoTemplate();
				infoTemplate.setTitle("Shelter Info Window");
				infoTemplate.setContent(setInfoTemplate);
			
			//only provide shelter functions if the shelter is owned by the user	
			function setInfoTemplate(graphic){

				
				var attr = graphic.attributes;
	
				templateContent = attr.name + " is " + attr.statuscolour + ": " + attr.available + "/" + attr.capacity +  " beds available.<br/>"
				// if(attr.Creator == app_Settings.userId){
					// templateContent += "<button onclick=editEntry(\"" + attr.OBJECTID + "\") class='btn btn-primary btn-block'>EDIT</button>"
				// }         
			    return templateContent
			}

            // Add the FeatureLayer for the Shelters...
            // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1
			
            // TODO: move URI for layer into JSON config file.
            app_Settings.shelterLayer = new FeatureLayer(app_Settings.layerURL, {
                mode: FeatureLayer.MODE_SNAPSHOT,
                infoTemplate: infoTemplate,
                outFields: ["*"]
            });
 			//set the symbol used for drawing shelters
			var defaultSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_White.png","contentType":"image/png","width":24,"height":24});
			var redSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Red.png","contentType":"image/png","width":24,"height":24});
			var greenSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Green.png","contentType":"image/png","width":24,"height":24});
			var yellowSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Yellow.png","contentType":"image/png","width":24,"height":24});
			var renderer = new esri.renderer.UniqueValueRenderer(defaultSymbol, "statuscolour");
			renderer.addValue("RED", redSymbol);
			renderer.addValue("YELLOW", yellowSymbol);
			renderer.addValue("GREEN", greenSymbol);
			app_Settings.shelterLayer.setRenderer(renderer);
			
            // add the layer
            app_Settings.map.addLayer(app_Settings.shelterLayer);
            //shelterLayer.infoTemplate = popupTemplate;
            // end - Shelter Layer & Popup
			d1.resolve();

          //legend.startup();
        });
        
		console.log("setting viz for sign-in/out");

        domStyle.set("sign-in", "display", "none");
        domStyle.set("sign-out", "display", "none");
        domStyle.set("userName", "display", "none");

        // OAuth2 login info:
        var info = new ArcGISOAuthInfo({
        appId: app_Settings.appId,
          // Uncomment this line to prevent the user's signed in state from being shared
          // with other apps on the same domain with the same authNamespace value.
          //authNamespace: "portal_oauth_inline",
          popup: false
        });
        esriId.registerOAuthInfos([info]);

        esriId.checkSignInStatus(info.portalUrl).then(
          function(cred) {
            domStyle.set("sign-in", "display", "none");
            domStyle.set("sign-out", "display", "inline");
            domStyle.set("userName", "display", "inline");
            // console.log(cred);
            console.log("User Signed In...");
            storeToken(cred.token, cred.userId)
            addOwnerButtons()
			d2.resolve();
          }
        ).otherwise(
          function() {
            // Anonymous view
            domStyle.set("sign-in", "display", "inline");
            domStyle.set("sign-out", "display", "none");
            domStyle.set("userName", "display", "none");
			storeToken(undefined, undefined)
            console.log("NOT SIGNED IN...");
            //domStyle.set("anonymousPanel", "display", "block");
            //domStyle.set("personalizedPanel", "display", "none");
            d2.resolve();
          }
        );
        // end OAuth2
        // hook up click handlers.
        on(dom.byId("sign-in"), "click", function() {
          console.log("click", arguments);
          // user will be redirected to OAuth Sign In page
          esriId.getCredential(info.portalUrl);
		     


        });

        on(dom.byId("sign-out"), "click", function() {
        	domStyle.set("userName", "display", "none");
          esriId.destroyCredentials();
          window.location.reload();
        });
		


      });
}


