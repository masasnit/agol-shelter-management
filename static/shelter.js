function getAjaxCall(urlSuffix, callback_success, callback_fail){
	url = app_Settings.layerURL + urlSuffix
	var request = $.ajax({
        type: 'POST',
        url: url,
        datatype:"application/json",
        Accept: "application/json",
        timeout: 120000
    });
    
    request.done( function(responseMsg, textStatus, jqXHR) {
		if( callback_success && typeof( callback_success ) === "function" )
	        {
	            callback_success( responseMsg );
	        }
    });

    request.fail( function(jqXHR, textStatus, errorThrown) {
	    console.log( jqXHR );
	    console.log( 'Fail status: ' + textStatus );
	    if( callback_fail && typeof( callback_fail ) === "function" )
        {
            callback_fail(jqXHR, textStatus, errorThrown );
        }

    });
}

function sendAjaxCall(urlSuffix, data, callback_success, callback_fail){
	url = app_Settings.layerURL + urlSuffix
	var request = $.ajax({
        type: 'POST',
        url: url,
        datatype:"application/x-www-form-urlencoded",
        Accept: "application/json",
        data: {"features": JSON.stringify(data)},
        timeout: 120000
    });
    
    request.done( function(responseMsg, textStatus, jqXHR) {
		// console.log( 'save info success ');
		if( callback_success && typeof( callback_success ) === "function" )
	        {
	            callback_success( responseMsg );
	        }
    });

    request.fail( function(jqXHR, textStatus, errorThrown) {
		console.log( jqXHR );
		console.log( 'save info Fail: ' + textStatus );
		 if( callback_fail && typeof( callback_fail ) === "function" )
	        {
	            callback_fail(jqXHR, textStatus, errorThrown );
	        }
    });
}


function sendToPython(entryData, urlSuffix,  callback_success, callback_fail){
	var url = urlSuffix;
	// console.log("POST URL: " + url);
    var request = $.ajax({
        type: 'POST',
        url: url,
        datatype:"application/json",
        data: entryData,
        timeout: 120000
    });
    
    request.done( function(responseMsg, textStatus, jqXHR) {
		// console.log( 'save info success ');
		if( callback_success && typeof( callback_success ) === "function" )
	        {
	            callback_success( responseMsg );
	        }
    });

    request.fail( function(jqXHR, textStatus, errorThrown) {
		console.log( jqXHR );
		console.log( 'save info Fail: ' + textStatus );
		 if( callback_fail && typeof( callback_fail ) === "function" )
	        {
	            callback_fail(jqXHR, textStatus, errorThrown );
	        }
    });
}

function getFromPython(entryData, urlSuffix,  callback_success, callback_fail){
	var url = urlSuffix;
	// console.log("POST URL: " + url);
    var request = $.ajax({
        type: 'GET',
        url: url,
        datatype:"application/json",
        data: entryData,
        timeout: 120000
    });
    
    request.done( function(responseMsg, textStatus, jqXHR) {
		// console.log( 'save info success ');
		if( callback_success && typeof( callback_success ) === "function" )
	        {
	            callback_success( responseMsg );
	        }
    });

    request.fail( function(jqXHR, textStatus, errorThrown) {
		console.log( jqXHR );
		console.log( 'save info Fail: ' + textStatus );
		 if( callback_fail && typeof( callback_fail ) === "function" )
	        {
	            callback_fail(jqXHR, textStatus, errorThrown );
	        }
    });
}