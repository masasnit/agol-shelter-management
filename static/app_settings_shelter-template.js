// declare the app_Settings
var app_Settings = {
	clickCreate : false,
    appId: "XXXXXX",
    layerURL : "https://services7.arcgis.com/XXXXXX/arcgis/rest/services/Shelters/FeatureServer/0",
    extentParams : {"xmin": -85, "ymin": 43, "xmax": -65, "ymax":47 },
    extentZoomFactor: 1000 //some factor for converting point to extent
};