//For use with the shelter manager tool.
var d1 = new $.Deferred();
var d2 = new $.Deferred();
function init(){
	createWebMap();
	$.when( d1, d2 ).done(function ( v1, v2 ) {
	    getLayerInfo()
	});
	var validator = new FormValidator('editShelterForm', [{
	    name: 'editShelterName',
	    display: 'Name',
	    rules: 'required'
	}, {
	    name: 'editShelterAvailable',
	   	display: 'Available',
	    rules: 'required|numeric'
	}, {
	    name: 'editShelterCapacity',
	   	display: 'Capacity',
	    rules: 'required|numeric'
	},{
	    name: 'editShelterActive',
	    display: 'Active',
	    rules: 'callback_checkActiveDropDown'
	}, {
	    name: 'editShelterStatus',
	    display: 'Status',
	    rules: 'callback_checkStatusDropDown'
	}, {
	    name: 'editShelterStatusColour',
	    display: 'Status Colour',
	    rules: 'callback_checkStatusColourDropDown'
	}], function(errors) {
	    if (errors.length > 0) {
	    	// console.log(errors)
	        for(var i = 0; i < errors.length; i++ ){
	        	// console.log(errors[i].message)
	        	$("#editShelterFormErrors").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errors[i].message + '</div>');
	        }
	        
	    }else{
	    	saveShelterInfo();
	    }
	});
	validator.registerCallback('checkStatusDropDown', function(value) {
	    if (value == "-- select an option --") {
	        return false;
	    }
	
	    return true;
	})
	.setMessage('checkStatusDropDown', 'Please select an option from the drop Status down menu');
	validator.registerCallback('checkStatusColourDropDown', function(value) {
	    if (value == "-- select an option --") {
	        return false;
	    }
	
	    return true;
	})
	.setMessage('checkStatusColourDropDown', 'Please select an option from the Status Colour drop down menu');
	validator.registerCallback('checkActiveDropDown', function(value) {
	    if (value == "-- select an option --") {
	        return false;
	    }
	
	    return true;
	})
	.setMessage('checkActiveDropDown', 'Please select an option from the Active drop down menu');
}

function getLayerInfo(){
	app_Settings.OBJECTID = getParameterByName('OBJECTID');
	url = "/query?where=OBJECTID=" + app_Settings.OBJECTID + "&outFields=*&returnGeometry=true&f=pjson&token="
	if(app_Settings.token != undefined){
		url = url + app_Settings.token
	}
	getAjaxCall(url,loadShelterEdits)
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


//load the edit fields with the information provided by ArcGIS Online
function loadShelterEdits(response){
	shelter = JSON.parse(response) //Parse the response
	// console.log(shelter)
	$("#editShelterName").val(shelter.features[0].attributes.name);
	$("#editShelterDescription").val(shelter.features[0].attributes.description);
	$("#editShelterActive").val(shelter.features[0].attributes.active);
	$("#editShelterAvailable").val(shelter.features[0].attributes.available);
	$("#editShelterCapacity").val(shelter.features[0].attributes.capacity);
	$("#editShelterStatus").val(shelter.features[0].attributes.status);
	$("#editShelterStatusColour").val(shelter.features[0].attributes.statuscolour);
	$("#editShelterLocation").val(shelter.features[0].attributes.locationName);
	$("#editShelterAlternatives").val(shelter.features[0].attributes.alternatives);
	pt = shelter.features[0].geometry
	var factor = app_Settings.extentZoomFactor; 
	var extent = new esri.geometry.Extent(pt.x - factor, pt.y - factor, pt.x + factor, pt.y + factor, new esri.SpatialReference({wkid:shelter.spatialReference.wkid}));
	//Set map extent based on app settings
	// console.log(app_Settings.map)
	app_Settings.map.setExtent(extent);
}



function editEntry(OBJECTID){
	window.location.replace("/edit/?OBJECTID=" + OBJECTID);
}

function saveShelterInfo(){
	url = '/updateFeatures?f=json&token=' + app_Settings.token
	feature = [{
      "attributes" : {
      	"OBJECTID" : app_Settings.OBJECTID,
        "name" : $("#editShelterName").val(), 
        "description" : $("#editShelterDescription").val(),
        "active" : $("#editShelterActive").val(), 
        "available" : $("#editShelterAvailable").val(), 
        "capacity" : $("#editShelterCapacity").val(), 
        "statuscolour" : $("#editShelterStatusColour").val(), 
        "locationName" : $("#editShelterLocation").val(), 
        "status" : $("#editShelterStatus").val(), 
        "alternatives" : $("#editShelterAlternatives").val()
      }
    }]
	sendAjaxCall(url, feature, saveEditsSuccess, saveEditsFail)
}

function saveEditsSuccess(response){
	console.log(response)
	if (response.indexOf("No permission") != -1){
		
		$("#saveEditsSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> You do not have permission to edit that shelter.</div>');
	}else if(response.indexOf("400") != -1){
		$("#saveEditsSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured.</div>');
	}else{
		$("#saveEditsSuccess").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Shelter info saved.</div>');
	}
	
}

function saveEditsFail(response){
	console.log(response)
	$("#saveEditsSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured.</div>');
}

function deleteShelterModal(){
	$('#deleteModal').modal('show')
}


function deleteShelter(){
	url = '/deleteFeatures?f=json&where=OBJECTID=' + app_Settings.OBJECTID + '&token=' + app_Settings.token
	feature = {}
	sendAjaxCall(url, feature , deleteSuccess, deleteFail)
}

function deleteSuccess(response){
	console.log(response)
	$('#deleteModal').modal('hide')
	if (response.indexOf("No permission") != -1){
		
		$("#saveEditsSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> You do not have permission to edit that shelter.</div>');
	}else if(response.indexOf("400") != -1){
		$("#saveEditsSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured while deleting shelter.</div>');
	}else{
		$("#saveEditsSuccess").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Shelter deleted.</div>');
	}
	
}

function deleteFail(response){
	console.log(response)
	$('#deleteModal').modal('hide')
	$("#saveEditsSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured while deleting shelter.</div>');
}


//Don't build the table until the token is stored.
function storeToken(token, userId){
	console.log("Storing Token")
	app_Settings.token = token
	app_Settings.userId = userId
	$("#userName").text("Welcome, " + userId);
}

function createWebMap(){
	var webmap = {};
    webmap.item = {
        "title": "Shelter Manager (by DRDC/Centre for Security Science",
        "snippet": "This map provides Shelter Management capabilities for multiple organizations sharing information in a single ArcGIS Online layer."
        //TODO: Add Extent via configuration?
    };

    webmap.itemData = {
        "baseMap": {
        "baseMapLayers": [{
          "opacity": 0.8,
          "visibility": true,
          "url":"https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"
          //"url": "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
          }],
        "title": "Topo..."
      },
      "version": "1.1"

    };

    var map;
    require([
      "esri/map",
      "esri/arcgis/utils",
      // Popup & Shelter Layer includes
      "esri/dijit/Geocoder", "esri/layers/FeatureLayer","esri/InfoTemplate","dojo/dom-style", "dojo/dom", "dojo/on", "esri/arcgis/OAuthInfo", "esri/IdentityManager","esri/graphic","esri/geometry/Point",
      "dojo/domReady!"],function(Map, arcgisUtils, Geocoder,  FeatureLayer, InfoTemplate, domStyle, dom, on, ArcGISOAuthInfo, esriId, Graphic, Point){
		
			
		  //Create the webmap
          arcgisUtils.createMap( webmap, "mapDiv").then(function (response) {
          app_Settings.map = response.map;
  		  //create address search bar
		  geocoder = new Geocoder({ 
            map: app_Settings.map 
          }, "search");
          geocoder.startup();
          // Create the custom popup for the Shelters.
          // get the Shelters layer.
            // var shelterLayer = app_Settings.map.getLayer(1); // TODO: Get layer configured - OR - load by Name (better)
            /*
            Fields to load:
            name, description, active, capacity, available, statuscolour, locationName, status, alternatives
            // only name & description here so far...
            */
            var infoTemplate = new InfoTemplate();
				infoTemplate.setTitle("Shelter Info Window");
				infoTemplate.setContent(setInfoTemplate);
			
			//only provide shelter functions if the shelter is owned by the user	
			function setInfoTemplate(graphic){

				
				var attr = graphic.attributes;
	
				templateContent = attr.name + " is " + attr.statuscolour + ": " + attr.available + "/" + attr.capacity +  " beds available.<br/>"
				if(attr.Creator == app_Settings.userId){
					templateContent += "<button onclick=editEntry(\"" + attr.OBJECTID + "\") class='btn btn-primary btn-block'>EDIT</button>"
				}         
			    return templateContent
			}

            // Add the FeatureLayer for the Shelters...
            // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1
			
            // TODO: move URI for layer into JSON config file.
            app_Settings.shelterLayer = new FeatureLayer(app_Settings.layerURL, {
                mode: FeatureLayer.MODE_SNAPSHOT,
                infoTemplate: infoTemplate,
                outFields: ["*"]
            });
 			//set the symbol used for drawing shelters
			var defaultSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_White.png","contentType":"image/png","width":24,"height":24});
			var redSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Red.png","contentType":"image/png","width":24,"height":24});
			var greenSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Green.png","contentType":"image/png","width":24,"height":24});
			var yellowSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"https://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Yellow.png","contentType":"image/png","width":24,"height":24});
			var renderer = new esri.renderer.UniqueValueRenderer(defaultSymbol, "statuscolour");
			renderer.addValue("RED", redSymbol);
			renderer.addValue("YELLOW", yellowSymbol);
			renderer.addValue("GREEN", greenSymbol);
			app_Settings.shelterLayer.setRenderer(renderer);
			
			//add click listener
			app_Settings.map.on("click", loadClickLocation);
            // add the layer
            app_Settings.map.addLayer(app_Settings.shelterLayer);
            //shelterLayer.infoTemplate = popupTemplate;
            // end - Shelter Layer & Popup

            // TODO: Assign symbols to the layer since we are hitting raw data this way.
			function loadClickLocation(event) {
				if(app_Settings.clickCreate == true){
					createShelter(event.mapPoint.x, event.mapPoint.y)
					var pt = new Point(event.mapPoint.x,event.mapPoint.y,app_Settings.map.spatialReference)
					var sms = defaultSymbol
					var attr = {"Xcoord":event.mapPoint.x,"Ycoord":event.mapPoint.y};
					var graphic = new Graphic(pt,sms,attr);
					app_Settings.map.graphics.add(graphic);
				}	    
			}
			d1.resolve();
          //legend.startup();
        });
        
		console.log("setting viz for sign-in/out");

        domStyle.set("sign-in", "display", "none");
        domStyle.set("sign-out", "display", "none");
        domStyle.set("userName", "display", "none");

        // OAuth2 login info:
        var info = new ArcGISOAuthInfo({
        appId: app_Settings.appId,
          // Uncomment this line to prevent the user's signed in state from being shared
          // with other apps on the same domain with the same authNamespace value.
          //authNamespace: "portal_oauth_inline",
          popup: false
        });
        esriId.registerOAuthInfos([info]);

        esriId.checkSignInStatus(info.portalUrl).then(
          function(cred) {
            domStyle.set("sign-in", "display", "none");
            domStyle.set("sign-out", "display", "inline");
            domStyle.set("userName", "display", "inline");
            // console.log(cred);
            console.log("User Signed In...");
            storeToken(cred.token, cred.userId)
			d2.resolve();
          }
        ).otherwise(
          function() {
            // Anonymous view
            domStyle.set("sign-in", "display", "inline");
            domStyle.set("sign-out", "display", "none");
            domStyle.set("userName", "display", "none");
			storeToken(undefined, undefined)
            console.log("NOT SIGNED IN...");
            d2.resolve();
            //domStyle.set("anonymousPanel", "display", "block");
            //domStyle.set("personalizedPanel", "display", "none");
          }
        );
        // end OAuth2
        // hook up click handlers.
        on(dom.byId("sign-in"), "click", function() {
          console.log("click", arguments);
          // user will be redirected to OAuth Sign In page
          esriId.getCredential(info.portalUrl);
        });

        on(dom.byId("sign-out"), "click", function() {
        	domStyle.set("userName", "display", "none");
          esriId.destroyCredentials();
          window.location.reload();
        });
		
        // get the layer and create the custom popup.
        // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1

      });
}


