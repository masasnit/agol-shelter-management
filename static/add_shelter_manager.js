//For use with the shelter manager tool.
function init(){
	app_Settings.clickCreate = false
	$("[id='createShelterButton']").bootstrapSwitch();
	$('input[id="createShelterButton"]').on('switchChange.bootstrapSwitch', function(event, state) {
		  toggleClickCreate(state)
		});
	createWebMap()
	var validator = new FormValidator('addShelterForm', [{
	    name: 'addShelterName',
	    display: 'Name',
	    rules: 'required'
	}, {
	    name: 'addShelterAvailable',
	   	display: 'Available',
	    rules: 'required|numeric'
	}, {
	    name: 'addShelterCapacity',
	   	display: 'Capacity',
	    rules: 'required|numeric'
	},{
	    name: 'addShelterActive',
	    display: 'Active',
	    rules: 'callback_checkActiveDropDown'
	}, {
	    name: 'addShelterStatus',
	    display: 'Status',
	    rules: 'callback_checkStatusDropDown'
	}, {
	    name: 'addShelterStatusColour',
	    display: 'Status Colour',
	    rules: 'callback_checkStatusColourDropDown'
	}, {
	    name: 'addShelterX',
	    display: 'Geometry',
	    rules: 'required'
	}], function(errors) {
	    if (errors.length > 0) {
	    	// console.log(errors)
	        for(var i = 0; i < errors.length; i++ ){
	        	// console.log(errors[i].message)
	        	$("#createShelterFormErrors").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errors[i].message + '</div>');
	        }
	        
	    }else{
	    	addShelterInfo();
	    }
	});
	validator.registerCallback('checkStatusDropDown', function(value) {
	    if (value == "-- select an option --") {
	        return false;
	    }
	
	    return true;
	})
	.setMessage('checkStatusDropDown', 'Please select an option from the drop Status down menu');
	validator.registerCallback('checkStatusColourDropDown', function(value) {
	    if (value == "-- select an option --") {
	        return false;
	    }
	
	    return true;
	})
	.setMessage('checkStatusColourDropDown', 'Please select an option from the Status Colour drop down menu');
	validator.registerCallback('checkActiveDropDown', function(value) {
	    if (value == "-- select an option --") {
	        return false;
	    }
	
	    return true;
	})
	.setMessage('checkActiveDropDown', 'Please select an option from the Active drop down menu');
}

//Toggle the crosshairs and ability to click to create a shelter
function toggleClickCreate(state){
	//add click listener
	if(state){
		app_Settings.map.setMapCursor("crosshair");
		app_Settings.clickCreate = true
	}else{
		app_Settings.map.setMapCursor("default");
		app_Settings.clickCreate = false
	}
	
}


function createShelter(xVal, yVal){
	//store pre converted values for creation later
	app_Settings.xVal = xVal;
	app_Settings.yVal = yVal;
	var fieldPromise = convertToLatLon(xVal, yVal);
	//convert geometry for UI
	fieldPromise.done(function(result){
		// shelter = JSON.stringify(result);
		$("#addShelterX").val(result[0].x);
		$("#addShelterY").val(result[0].y);
	});
}

//Add shelter to layer
function addShelterInfo(){
	
	feature = [{
      "attributes" : { 
        "name" : $("#addShelterName").val(), 
        "description" : $("#addShelterDescription").val(),
        "active" : $("#addShelterActive").val(), 
        "available" : $("#addShelterAvailable").val(), 
        "capacity" : $("#addShelterCapacity").val(), 
        "statuscolour" : $("#addShelterStatusColour").val(), 
        "locationName" : $("#addShelterLocation").val(), 
        "status" : $("#addShelterStatus").val(), 
        "alternatives" : $("#addShelterAlternatives").val()
      }, 
      "geometry" : 
      {
      	//load geometry from pre converted values
        "x" : app_Settings.xVal, 
        "y" : app_Settings.yVal
      }
    }]
	url = '/addFeatures?f=json&token=' + app_Settings.token
	sendAjaxCall(url, feature, createBitly, addShelterFail)
	//sendAjaxCall(url, feature, addShelterSuccess, addShelterFail)
}

//Tell the user the creation was successful or failed
function addShelterSuccess(response){
	console.log(response)
	if(response.indexOf("true") > -1){
		$("#createShelterSuccess").append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Success!</strong> Shelter saved.</div>');
		app_Settings.shelterLayer.refresh()
	}else{
		$("#createShelterSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured.</div>');
	}
	
}

//Tell the user the creation was successful or failed
function addShelterFail(response){
	console.log(response)
	$("#saveModalSuccess").append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Failure!</strong> An error occured.</div>');
}


//build a tweet/chatter message by pulling the info from the stored table
function createBitly(response){
	var jsonResponse = JSON.parse(response);
	app_Settings.OBJECTID = jsonResponse["addResults"][0]["objectId"]
	var longurl = {}
	var pathname = $(location).attr('pathname')
	pathname = pathname.replace('add/','');
	longurl.viewURL = $(location).attr('protocol') + "//" + $(location).attr('hostname') + pathname + "view/?OBJECTID=" + app_Settings.OBJECTID
	var entryData = {"longurl" : JSON.stringify(longurl)}
	sendToPython(entryData,"/generator", createBitlySuccess, addShelterFail)
}

//report back the bitly url and update the shelter data
function createBitlySuccess(url){
	// alert(url)
	shorturl = url.trim()
	// alert(shorturl)
	url = '/updateFeatures?f=json&token=' + app_Settings.token
	feature = [{
      "attributes" : {
      	"OBJECTID" : app_Settings.OBJECTID,
        "link" : shorturl
      }
    }]
	sendAjaxCall(url, feature, addShelterSuccess, addShelterFail)
}



//Store login information and welcome the user
function storeToken(token, userId){
	console.log("Storing Token")
	app_Settings.token = token
	app_Settings.userId = userId
	$("#userName").text("Welcome, " + userId);
}

//Main function to create the webmap
function createWebMap(){
	var webmap = {};
    webmap.item = {
        "title": "Shelter Manager (by DRDC/Centre for Security Science",
        "snippet": "This map provides Shelter Management capabilities for multiple organizations sharing information in a single ArcGIS Online layer."
    };

    webmap.itemData = {
        "baseMap": {
        "baseMapLayers": [{
          "opacity": 0.8,
          "visibility": true,
          "url":"https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer"
          //"url": "http://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
          }],
        "title": "Topo..."
      },
      "version": "1.1"

    };

    var map;
    require([
      "esri/map",
      "esri/arcgis/utils",
      // Popup & Shelter Layer includes
      "esri/dijit/Geocoder", "esri/layers/FeatureLayer","esri/InfoTemplate","dojo/dom-style", "dojo/dom", "dojo/on", "esri/arcgis/OAuthInfo", "esri/IdentityManager","esri/graphic","esri/geometry/Point",
      "dojo/domReady!"],function(Map, arcgisUtils, Geocoder,  FeatureLayer, InfoTemplate, domStyle, dom, on, ArcGISOAuthInfo, esriId, Graphic, Point){
		
			
		  //Create the webmap
          arcgisUtils.createMap( webmap, "mapDiv").then(function (response) {
          app_Settings.map = response.map;
          //var legend = new Legend({
          //  map: map,
          //  layerInfos:(arcgisUtils.getLegendLayers(response))
          //}, "legendDiv");
          var startExtent = new esri.geometry.Extent(app_Settings.extentParams.xmin, app_Settings.extentParams.ymin, app_Settings.extentParams.xmax, app_Settings.extentParams.ymax, new esri.SpatialReference({wkid:4326}) );
		  //Set map extent based on app settings
  		  app_Settings.map.setExtent(startExtent);
  		  //create address search bar
		  geocoder = new Geocoder({ 
            map: app_Settings.map 
          }, "search");
          geocoder.startup();
          // Create the custom popup for the Shelters.
          // get the Shelters layer.
            // var shelterLayer = app_Settings.map.getLayer(1); // TODO: Get layer configured - OR - load by Name (better)
            /*
            Fields to load:
            name, description, active, capacity, available, statuscolour, locationName, status, alternatives
            // only name & description here so far...
            */
            // var infoTemplate = new InfoTemplate();
				// infoTemplate.setTitle("Shelter Info Window");
				// infoTemplate.setContent(setInfoTemplate);
// 				
			// function setInfoTemplate(graphic){
				// console.log("Setting template")
// 				
				// var attr = graphic.attributes;
				// console.log(attr.Creator + " equals " + app_Settings.userId)
// 	
				// templateContent = attr.name + " is " + attr.statuscolour + ": " + attr.available + "/" + attr.capacity +  " beds in use.<br/>"
				// if(attr.Creator == app_Settings.userId){
					// templateContent += "<button onclick=editEntry(\"" + attr.OBJECTID + "\") class='btn btn-primary btn-block'>EDIT</button>"
			        // templateContent += "<button onclick=buildTweet(\"" + attr.OBJECTID + "\") class='btn btn-info btn-block'>TWEET</button>"
				// }
// 			                  
			    // return templateContent
			// }

            // Add the FeatureLayer for the Shelters...
            // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1
			
            // TODO: move URI for layer into JSON config file.
            app_Settings.shelterLayer = new FeatureLayer(app_Settings.layerURL, {
                mode: FeatureLayer.MODE_SNAPSHOT,
                // infoTemplate: infoTemplate,
                outFields: ["*"]
            });
 			//set the symbol used for drawing shelters
			var defaultSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"http://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_White.png","contentType":"image/png","width":24,"height":24});
			var redSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"http://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Red.png","contentType":"image/png","width":24,"height":24});
			var greenSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"http://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Green.png","contentType":"image/png","width":24,"height":24});
			var yellowSymbol = new esri.symbol.PictureMarkerSymbol({"angle":0,"xoffset":0,"yoffset":0,"type":"esriPMS","url":"http://static.arcgis.com/images/Symbols/Transportation/esriBusinessMarker_74_Yellow.png","contentType":"image/png","width":24,"height":24});
			var renderer = new esri.renderer.UniqueValueRenderer(defaultSymbol, "statuscolour");
			renderer.addValue("RED", redSymbol);
			renderer.addValue("YELLOW", yellowSymbol);
			renderer.addValue("GREEN", greenSymbol);
			app_Settings.shelterLayer.setRenderer(renderer);
			
			//add click listener
			app_Settings.map.on("click", loadClickLocation);
            // add the layer
            app_Settings.map.addLayer(app_Settings.shelterLayer);
            //shelterLayer.infoTemplate = popupTemplate;
            // end - Shelter Layer & Popup
			
            // TODO: Assign symbols to the layer since we are hitting raw data this way.
			function loadClickLocation(event) {
				//only create on click when toggled to true
				if(app_Settings.clickCreate){
					createShelter(event.mapPoint.x, event.mapPoint.y)
					var pt = new Point(event.mapPoint.x,event.mapPoint.y,app_Settings.map.spatialReference)
					var sms = defaultSymbol
					var attr = {"Xcoord":event.mapPoint.x,"Ycoord":event.mapPoint.y};
					var graphic = new Graphic(pt,sms,attr);
					app_Settings.map.graphics.clear()
					app_Settings.map.graphics.add(graphic);
				}
			}

          //legend.startup();
        });
        
		console.log("setting viz for sign-in/out");

        domStyle.set("sign-in", "display", "none");
        domStyle.set("sign-out", "display", "none");
        domStyle.set("userName", "display", "none");
        domStyle.set("createShelterButton", "display", "none");

        // OAuth2 login info:
        var info = new ArcGISOAuthInfo({
        appId: app_Settings.appId,
          // Uncomment this line to prevent the user's signed in state from being shared
          // with other apps on the same domain with the same authNamespace value.
          //authNamespace: "portal_oauth_inline",
          popup: false
        });
        esriId.registerOAuthInfos([info]);

        esriId.checkSignInStatus(info.portalUrl).then(
          function(cred) {
            domStyle.set("sign-in", "display", "none");
            domStyle.set("sign-out", "display", "inline");
            domStyle.set("userName", "display", "inline");
            domStyle.set("createShelterButton", "display", "inline");
            // console.log(cred);
            console.log("User Signed In...");
            storeToken(cred.token, cred.userId)
			
          }
        ).otherwise(
          function() {
            // Anonymous view
            domStyle.set("sign-in", "display", "inline");
            domStyle.set("sign-out", "display", "none");
            domStyle.set("userName", "display", "none");
            domStyle.set("createShelterButton", "display", "none");
			storeToken(undefined, undefined)
            console.log("NOT SIGNED IN...");
            //domStyle.set("anonymousPanel", "display", "block");
            //domStyle.set("personalizedPanel", "display", "none");
          }
        );
        // end OAuth2
        // hook up click handlers.
        on(dom.byId("sign-in"), "click", function() {
          console.log("click", arguments);
          // user will be redirected to OAuth Sign In page
          esriId.getCredential(info.portalUrl);

        });

        on(dom.byId("sign-out"), "click", function() {
        	domStyle.set("userName", "display", "none");
        	domStyle.set("createShelterButton", "display", "none");
          esriId.destroyCredentials();
          window.location.reload();
        });
		
        // get the layer and create the custom popup.
        // http://services1.arcgis.com/Nt1b6lBKgKX3wost/arcgis/rest/services/SheltersCRC/FeatureServer/1

      });
}


function convertToLatLon(inlat, inlon) {
  var dff = $.Deferred();
  require([
      "esri/tasks/GeometryService",
      "esri/tasks/ProjectParameters",
		],function(GeometryService, ProjectParameters){
		var incoord = 102100
		  var outcoord = 4326
		  var datumtrans = 'Default'
		  if (isNaN(inlat) | isNaN(inlon)) {
		    alert("Please enter valid numbers");
		  } else {
		    var inSR = new esri.SpatialReference({
		      wkid: incoord
		    });
		    var outSR = new esri.SpatialReference({
		      wkid: outcoord
		    });
		    var geometryService = new esri.tasks.GeometryService("http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
		    var inputpoint = new esri.geometry.Point(inlon, inlat, inSR);
		    var PrjParams = new esri.tasks.ProjectParameters();
		    PrjParams.geometries = [inputpoint];
		    PrjParams.outSR = outSR;
		    if (datumtrans != 'Default') {
		      PrjParams.transformation = {
		        wkid: parseInt(datumtrans)
		      }
		    };
		    geometryService.project(PrjParams, function (outputpoint) {
		      dff.resolve(outputpoint);
		      
		    });
		  }
      });
      return dff.promise();
}