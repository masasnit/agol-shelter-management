#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           Shelter Manager
Author:         Darrell O'Donnell
Created:        15SEP2014
Updated:        16SEP2014
Copyright:      Copyright (c) 2014 - Her Majesty, in Right of Canada, DRDC/CSS
License:        see license.txt
Description:    Basic API to handle server-side functions for Shelter Management App

                Principally sending status updates to social media endpoings (twitter or chatter)
                    - correlates AGOL userid with stored token
"""
import os
import sys
import optparse
import logging
import logging.handlers
from pyproj import Proj, transform
from datetime import datetime, timedelta
import time, random, collections, hmac, binascii
from hashlib import sha1

import urllib2
import urllib

try:
    import json
except ImportError:
    import simplejson as json

import cherrypy
from cherrypy.lib import sessions

import logging
from logging.handlers import RotatingFileHandler

from Entry.Entry import Entry
from Entry.Link import Link
from geojson import Point, LineString, Polygon, MultiPolygon

import bitly_api
import tweepy


#TODO: Consider old INTEGER values and UUID based values - what will be returned????

# TODO: MOVE CONFIG INFO INTO CONFIG FILE...
cherrypy_app = {"/": {
    
    "tools.staticdir.root": "",
    "tools.staticdir.on": True, "tools.staticdir.dir": ".",
    # use a custom tool for IP address rewriting when behind a proxy
#    "tools.xff.on": True,
#    "tools.xff.debug": True,
    },
}

current_dir = os.path.dirname(os.path.abspath(__file__))

templateFolder = os.path.join(current_dir, 'templates')
staticFolder = os.path.join(current_dir, 'static')



usage = "usage: python shelter.py [-c] config"
parser = optparse.OptionParser(usage)

parser.add_option("-c", action="append", type="string", dest="config_file",
                  help="read config from this python module")
(options, args) = parser.parse_args()

# don't catch config exceptions, let them show on commandline
app_config = {}
if options.config_file:
    # currently uses the first string provided by options and can
    # be relative or absolute url to .py file
    print "loading %s" % options.config_file[0]
    execfile(options.config_file[0], {}, app_config)
else:
    # use the default config file
    print "loading config-debug.py"
    execfile("config-debug.py", {}, app_config) 

# set up LOGGING
# SET UP LOGGING - located in this config to make admin simpler. :-)



loggers = app_config["logging_info"]["loggers"]

logger = logging.getLogger('acv11api')
logger.setLevel(app_config["logging_info"]["loglevel"]) 
                
# create formatter and add it to the handlers
formatter = logging.Formatter(app_config["logging_info"]["logKEY"] + ' %(asctime)s - %(levelname)s - %(message)s')

# file-based log
if "file" in loggers:
    app_dir = os.path.dirname(sys.argv[0])
    log_path = os.path.join(current_dir, app_config["logging_info"]["logfile"])
    print log_path

    fh = RotatingFileHandler(log_path, maxBytes=100000, backupCount=10)
#     fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

# console log
if "console" in loggers:
    ch = logging.StreamHandler()
#     ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    
# syslog
if "syslog" in loggers:
    # hook up the syslog.
    if "logfile" in app_config["logging_info"]:
        print "Creating log @ %s " % app_config["logging_info"]["syslogaddress"]
        sl = logging.handlers.SysLogHandler(app_config["logging_info"]["syslogaddress"])
#         sl.setLevel(logging.INFO)
        logger.addHandler(sl)



logger.info(" Starting - logger attached.")
        


class Root(object):
    @cherrypy.expose
    def index(self, **params):
        logger.debug("about to load index.html template")
        return open(os.path.join(staticFolder, 'shelter.html'))
    
    @cherrypy.expose
    def add(self, **params):
        
        return open(os.path.join(staticFolder, 'addShelter.html'))
    @cherrypy.expose
    def edit(self, **params):
        
        return open(os.path.join(staticFolder, 'editShelter.html'))

    @cherrypy.expose
    def view(self, **params):
        
        return open(os.path.join(staticFolder, 'viewShelter.html'))

class SendUpdate(object):
    """ queries LDAP to see if the access code provided has access """

    exposed = True


    def GET(self, message="", agoluserid="", latitude=-1000, longitude=-1000, _dc="", *args):
        print "HEADERS: %s" %(cherrypy.request.headers)

        logger.debug("message: %s" % message)
        logger.debug("@lat,long: %s, %s" % (latitude, longitude))
        logger.debug("user: %s" % agoluserid)
        params = {'content': message, 'latitude': latitude, 'longitude': longitude}
        parms = urllib.urlencode(params)
        logger.debug("parms: %s" % parms)

        #TODO: Link agoluserid to the stored token:
          # for Chatter - just use a config file that we maintain manually {"agoluserid":"chattertoken"}

        # send to Chatter
#         fullURL = "http://chatter.responseready.ca/api.php?action=newPost&token=d13ad979b6d5f8f5d7deddc595993381&" + parms
#         logger.debug("fullURL: %s" % fullURL)
#         urllib2.urlopen(fullURL).read() # chatter works with GET or POST...

class CreateURL(object):
    """ Receives the tweet from javascript and processes it here """
    
    exposed = True
    @cherrypy.tools.accept(media='application/json')
    def POST(self, longurl=None):
        jsonData = json.loads(longurl)

        # use Bit.ly to generate URL if it is enabled
        if app_config["bitlyAccount"]["enabled"] == True:
          print("BITLY URL??????")
          #Log into bitly
          conn_btly = bitly_api.Connection(access_token=app_config["bitlyAccount"]["token"])

          #create short bitly URL
          shortBitly = conn_btly.shorten(jsonData["viewURL"])

          return shortBitly["url"]
        else:
          print("entry Link URL: " + jsonData["viewURL"])
          return jsonData["viewURL"] # return the long URL that was passed in.



class BuildMasasEntry(object):
    """ Receives the MASAS information from javascript and processes it here """
    
    exposed = True
    @cherrypy.tools.accept(media='application/json')
    def POST(self, masas=None):
        jsonData = json.loads(masas)
        #Set the geometry
        inProj = Proj(init="epsg:3857")
        outProj = Proj(init='epsg:4326')
        x1,y1 = jsonData["geometry"]["x"],jsonData["geometry"]["y"]
        x2,y2 = transform(inProj,outProj,x1,y1)
        # Create the Entry...
        entry = Entry()
        url = ""
        #Check AGOL to make sure Id doesn't already exist
        agol_url = app_config["layerURL"] + "/query?where=OBJECTID=" + jsonData["OBJECTID"] + "&outFields=masasId&f=json"
        response = urllib2.urlopen(agol_url).read()
        agolJSON = json.loads(response)

        if agolJSON["features"][0]["attributes"]["masasId"] == None: 
            #Add Categories
            entry.categories.append("Safety")
            entry.status = "Actual"
            entry.icon = "ems/operations/emergency/emergencyShelter"
            url = app_config["hubURL"] + "?secret=" + app_config["masasAccount"][jsonData["userId"]]
            # url = "https://sandbox2.masas-sics.ca/hub/feed?secret=" + app_config["masasAccount"][jsonData["userId"]]
            req = urllib2.Request(url)
        elif jsonData["masasId"] == None:
            return ""
        else:
            url = app_config["hubURL"] + jsonData["masasId"] + "?secret=" + app_config["masasAccount"][jsonData["userId"]]
            # url = "https://sandbox2.masas-sics.ca/hub/feed/" + jsonData["masasId"] + "?secret=" + app_config["masasAccount"][jsonData["userId"]]
            response = urllib2.urlopen(url).read()

            logger.info(response)

            entry.setDataFromXML(response)
            req = urllib2.Request(url)
            req.get_method = lambda: 'PUT'
        entry.setContent(jsonData["description"])
        entry.setTitle(jsonData["name"] + ' is ' + jsonData["status"] + ': ' + str(jsonData["available"]) + '/' + str(jsonData["capacity"]) + ' beds ')

        bitly = Link()
        bitly.href = jsonData["link"]
        bitly.rel = "related"
        entry.links.append(bitly)

        entry.geometry = Point((y2, x2))
        entry.expires = datetime.utcnow() + timedelta( minutes = app_config["masasExpiresMinutes"] )
        req.add_header('Content-Type', 'application/atom+xml')
        req.add_data(entry.toXML())
        #Get the new created entry
        response = urllib2.urlopen(req).read()
        createdEntry = Entry()
        #logger.info("RAW MASAS ENTRY: " + response)
        createdEntry.setDataFromXML(response)
        return createdEntry.id + ":" + jsonData["OBJECTID"]

#Authenticate a user with twitter    
class authenticate(object):
    
    exposed = True
    def GET(self):    
        cherrypy.session.cache.clear()
        cherrypy.session['oauth_secret'] = ''
        
        #Request Params for twitter auth
        requestParams = {
            "oauth_callback" : app_config["baseURL"] + "/authorised",
            "oauth_consumer_key" : app_config["twitterSettings"]["consumer_key"],
            "oauth_nonce" : str(random.randint(1, 999999999)),
            "oauth_signature_method" : "HMAC-SHA1",
            "oauth_timestamp" : int(time.time()),
            "oauth_version" : "1.0"
        }
    
        theSig = sign_request(requestParams, "POST", "https://api.twitter.com/oauth/request_token")
    
        requestParams["oauth_signature"] = theSig
    
        request = urllib2.Request("https://api.twitter.com/oauth/request_token", "")
        request.add_header("Authorization", create_oauth_headers(requestParams))
    
        try:
            httpResponse = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            return e.read()
    
        responseData = getParameters(httpResponse.read())
        
        #Store the token and secret in session data for later
        cherrypy.session['oauth_token'] = responseData['oauth_token']
        cherrypy.session['oauth_secret'] = responseData['oauth_token_secret']
    
        raise cherrypy.HTTPRedirect("https://api.twitter.com/oauth/authorize?oauth_token=" + cherrypy.session['oauth_token'])

#Second part of the handshake process for authentication
class authorised(object):
    
    exposed = True
    def GET(self, oauth_token="", oauth_verifier=""):   
        
        if oauth_token == cherrypy.session['oauth_token']:
            #Request Params for twitter auth
            verifyRequestParams = {
                "oauth_consumer_key" : app_config["twitterSettings"]["consumer_key"],
                "oauth_nonce" : str(random.randint(1, 999999999)),
                "oauth_signature_method" : "HMAC-SHA1",
                "oauth_timestamp" : int(time.time()),
                "oauth_token" : cherrypy.session['oauth_token'],
                "oauth_version" : "1.0"
            }
    
            signVerification = sign_request(verifyRequestParams, "POST", "https://api.twitter.com/oauth/access_token")
    
            verifyRequestParams["oauth_signature"] = signVerification
            verifyRequest = urllib2.Request("https://api.twitter.com/oauth/access_token", "oauth_verifier=" + oauth_verifier)
            verifyRequest.add_header("Authorization", create_oauth_headers(verifyRequestParams))
            
            try:
                httpResponse = urllib2.urlopen(verifyRequest)
            except urllib2.HTTPError, e:
                return e.read()
            responseData = getParameters(httpResponse.read())
            #Store the new token and secret in session data for later
            cherrypy.session['oauth_token'] = responseData["oauth_token"]
            cherrypy.session['oauth_secret'] = responseData["oauth_token_secret"]
            raise cherrypy.HTTPRedirect("/")

#Uses tweepy to send a twitter with the users token stored in session data    
class sendTweet(object):
    
    exposed = True
    @cherrypy.tools.accept(media='application/json')
    def POST(self, tweetData=""):
        
        #If user isn't logged in then redirect to login page
        if "oauth_token" in cherrypy.session:
            jsonData = json.loads(tweetData)
            
            #Convert the geometry into what twitter expects
            geom = jsonData["geom"].split("|")
            inProj = Proj(init="epsg:3857")
            outProj = Proj(init='epsg:4326')
            x1,y1 = geom[0],geom[1]
            x2,y2 = transform(inProj,outProj,x1,y1)
            
            #Authenticate against tweepy
            auth = tweepy.OAuthHandler(app_config["twitterSettings"]["consumer_key"], app_config["twitterSettings"]["consumer_secret"])
            auth.set_access_token(cherrypy.session['oauth_token'], cherrypy.session['oauth_secret'])
            
            api = tweepy.API(auth)
            
            #Post new tweet to twitter
            api.update_status(jsonData["status"], long=x2, lat=y2)

        else:
            raise cherrypy.HTTPRedirect("/authenticate")

#Check to insure user is logged into twitter and passes back username, OBJECTID and geolocation information
class checkTweet(object):
    
    exposed = True
    @cherrypy.tools.accept(media='application/json')
    def GET(self, OBJECTID=""):
        
        if "oauth_token" in cherrypy.session:
            #Request Params for twitter auth
            verifyRequestParams = {
                "oauth_consumer_key" : app_config["twitterSettings"]["consumer_key"],
                "oauth_nonce" : str(random.randint(1, 999999999)),
                "oauth_signature_method" : "HMAC-SHA1",
                "oauth_timestamp" : int(time.time()),
                "oauth_token" : cherrypy.session['oauth_token'],
                "oauth_version" : "1.0"
            }
    
            signVerification = sign_request(verifyRequestParams, "GET", "https://api.twitter.com/1.1/account/settings.json")
    
            verifyRequestParams["oauth_signature"] = signVerification
            verifyRequest = urllib2.Request("https://api.twitter.com/1.1/account/settings.json")
            verifyRequest.add_header("Authorization", create_oauth_headers(verifyRequestParams))
            
            try:
                httpResponse = urllib2.urlopen(verifyRequest)
            except urllib2.HTTPError, e:
                return e.read()
            jsonData = json.loads(httpResponse.read())
            
            #Create json string for front end to use
            return '{"success": "True", "OBJECTID": "' + OBJECTID + '","screen_name": "' + jsonData["screen_name"] + '","geo_enabled": "' + str(jsonData["geo_enabled"]) + '"}'
        else:
            #Report back login was unsuccessful
            return '{"success": "False"}'

def getParameters(paramString):

    paramString = paramString.split("&")

    pDict = {}

    for parameter in paramString:
        parameter = parameter.split("=")

        pDict[parameter[0]] = parameter[1]

    return pDict

def sign_request(parameters, method, baseURL):

    baseURL = urllib.quote(baseURL, '')

    p = collections.OrderedDict(sorted(parameters.items(), key=lambda t: t[0]))

    requestString = method + "&" + baseURL + "&"
    parameterString = ""

    for idx, key in enumerate(p.keys()):
        paramString = key + "=" + urllib.quote(str(p[key]), '')
        if idx < len(p.keys()) - 1:
            paramString += "&"

        parameterString += paramString

    result = requestString + urllib.quote(parameterString, '')

    signingKey = app_config["twitterSettings"]["consumer_secret"] + "&" + cherrypy.session['oauth_secret']


    hashed = hmac.new(signingKey, result, sha1)
    signature = binascii.b2a_base64(hashed.digest())[:-1]

    return signature

def create_oauth_headers(oauthParams):
    
    oauthp = collections.OrderedDict(sorted(oauthParams.items(), key=lambda t: t[0]))

    headerString = "OAuth "

    for idx, key in enumerate(oauthp):
        hString = key + "=\"" + urllib.quote(str(oauthp[key]), '') + "\""
        if idx < len(oauthp.keys()) - 1:
            hString += ","

        headerString += hString
    return headerString


def run():

    """ run method for standalone Hub server """

    logger.debug("run() ... STARTING UP.")

    # set the CherryPy Server level info from the config.
    cherrypy.config.update(app_config["cherrypy_server"])


#     print current_dir

    # ASSUMPTION: all static files are in same folder under /static
    # if more are added then we need to adjust accordingly.

    logger.debug("static folder: %s" % staticFolder)
    conf = {'/static':
                {'tools.staticdir.on': True,
                'tools.staticdir.dir': staticFolder,
                'tools.staticdir.index' : 'shelter.html',
                'tools.sessions.on': True,
            },    
            '/generator': {
                 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                 'tools.response_headers.on': True,
                 'tools.response_headers.headers': [('Content-Type', 'text/plain')],
                 'tools.sessions.on': True,
             },
            '/masas': {
                 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                 'tools.response_headers.on': True,
                 'tools.response_headers.headers': [('Content-Type', 'text/plain')],
             },
            '/authenticate': {
                 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                 'tools.response_headers.on': True,
                 'tools.sessions.on': True,
                 'tools.response_headers.headers': [('Content-Type', 'text/plain')],
             },
            '/authorised': {
                 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                 'tools.response_headers.on': True,
                 'tools.sessions.on': True,
                 'tools.response_headers.headers': [('Content-Type', 'text/plain')],
             },
            '/sendTweet': {
                 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                 'tools.response_headers.on': True,
                 'tools.sessions.on': True,
                 'tools.response_headers.headers': [('Content-Type', 'text/plain')],
             },
            '/checkTweet': {
                 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                 'tools.response_headers.on': True,
                 'tools.sessions.on': True,
                 'tools.response_headers.headers': [('Content-Type', 'text/plain')],
             },
            }


# mount the basic Access Checker
    cherrypy.tree.mount(
        SendUpdate(), '/sendupdate',
        {"/":
            {'request.dispatch': cherrypy.dispatch.MethodDispatcher()}
        }
    )

    root = Root()
    root.generator = CreateURL()
    root.masas = BuildMasasEntry()
    root.authenticate = authenticate()
    root.authorised = authorised()
    root.sendTweet = sendTweet()
    root.checkTweet = checkTweet()
    cherrypy.quickstart(root,"/",config = conf)


if __name__ == '__main__':
    run()
