#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Name:           Shelter Manager Debug Config
Author:         Darrell O'Donnell
Created:        15SEP2014
Updated:        2017-04-20
Copyright:      Copyright (c) 2014 - Her Majesty, in Right of Canada, DRDC/CSS
License:        see license.txt
Description:    config for debug purposes
"""
import logging  # used to set logging level

cherrypy_server = {
    # use default production environment settings such as no auto reloading,
    # hiding tracebacks from errors sent to users, and no stdout logging
    #    "environment": "production",
    # default is to server on localhost only, this opens up to other hosts,
    # comment out for production
    "server.socket_host": "0.0.0.0",
    "server.socket_port": 9999,
    # default is 100MB, set to 10MB here
    #     "server.max_request_body_size": 10485760,
    "log.access_file": "",
    # absolute path for the server log
    "log.error_file": "./server.log",
    # use for debugging with Winpdb to stop autoreloading
    #    "engine.autoreload_on": False,
}

cherrypy_app = {"/": {
    # set default header values for responses
    "tools.response_headers.on": True,
    "tools.response_headers.headers": [("Server", "Shelter Manager v0.1.1")],
    "tools.encode.on": True,
    "tools.encode.encoding": "utf-8"
    # a traceback is saved in the error log, this also saves HTTP headers
    #    "tools.log_headers.on": True,

}
}  # cherrypy_app

# baseURL is the value that is used internally in the service to map items back to this service.
baseURL = "http://XXXXXXXX"  # NOTE: no / at end



# The URL of the AGOL layer
layerURL = "https://services7.arcgis.com/XXXXXXXX/arcgis/rest/services/Shelters/FeatureServer/0"
# layerURL = "http://services1.arcgis.com/Nt1b6lBKgKX3wost/ArcGIS/rest/services/SheltersCRC/FeatureServer/2"

# System Logging - a file based (system.log is the default filename) log and
# a system-level log can be attached.
logging_info = {
    "logfile": "system.log",  # log will be stored in same folder as service.
    # .syslogaddress - system log name/location
    # Platform dependencies: MAC "/var/run/syslog", Linux "/dev/log"
    "syslogaddress": "/dev/log",
    # .loglevel - sets default log level for all handlers (can tailor below for each)
    "loglevel": logging.DEBUG,
    # Loggers to engage. Choices are: console, file, syslog
    "loggers": ["console", "file"],
    # .logKEY - display in log text (principally for syslog searching)
    "logKEY": "sheltermgr"

}

useBitly = false


# MASAS accounts to use based on ArcGIS usernames
masasAccount = {
    "agol_user_name": "masas_access_code"
}

hubURL = "https://sandbox2.masas-sics.ca/hub/feed/"
masasExpiresMinutes = 60

# Bitly Account to create smaller URLs
bitlyAccount = {
    "login": "XXXXXXXX",
    "token": "XXXXXXXX",
    "enabled": False
}

twitterSettings = {
    # Twitter OAuth Variables
    "consumer_key": "XXXXXXXX",
    "consumer_secret": "XXXXXXXX"
}
