
# Install Pip

https://pip.pypa.io/en/stable/installing/

# Install CherryPy

sudo pip install cherrypy

# Install pyproj

sudo pip install pyproj


# Install geojson

sudo pip install geojson

# Install bitly_api

sudo pip install bitly_api


# Install tweepy

NOTE: options required on macOS

```
sudo pip install tweepy --upgrade --ignore-installed six
```

```
sudo pip install enum
sudo pip install lxml


```

# Configure Python config

* layerURL: https://services7.arcgis.com/PnWcUejR9c4XQtT0/arcgis/rest/services/Shelters/FeatureServer/0
* bitlyAccount
* twitterSettings



# Configure .JS

In app_settings_shelter.js:

* appId: p7XR2KBEJn7PqF94
* layerURL: https://services7.arcgis.com/PnWcUejR9c4XQtT0/arcgis/rest/services/Shelters/FeatureServer/0
* bitlyAccount
* twitterSettings