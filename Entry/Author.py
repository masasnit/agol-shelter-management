import lxml.etree as ET

class Author( object ):

    def __init__( self ):
        self.name = ""
        self.uri = ""
        self.email = ""
    # end __init__
    
    #Returns Author as an XML element
    def toElementNode(self):
        author = ET.Element("author")
        name = ET.SubElement(author, "name")
        name.text = self.name
        uri = ET.SubElement(author, "uri")
        uri.text = self.uri
        email = ET.SubElement(author, "email")
        email.text = self.email
        return author
    # end toElementNode
    
    #Creates an Author from an XML element
    def fromElementNode(self, authorNode, namespace):
        try:
            self.name = authorNode.findtext('atom:name', namespaces=namespace)
            self.uri = authorNode.findtext('atom:uri', namespaces=namespace)
            self.email = authorNode.findtext('atom:email', namespaces=namespace)
        except AttributeError:
            self.name = ""
            self.uri = ""
            self.email = ""
    #end fromElementNode

# end Author