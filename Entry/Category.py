import lxml.etree as ET

class Category( object ):

    def __init__( self, label="", scheme="", term="" ):
        self.label = label
        self.scheme = scheme
        self.term = term
    # end __init__
    
    #Returns Category as an XML element
    def toElementNode(self):
        categoryNode = ET.Element("category", label=self.label, scheme=self.scheme, term=self.term)
        return categoryNode
    # end toElementNode

    #Creates a Category from an XML element
    def fromElementNode(self, categoryNode):
        try:
            self.label = categoryNode.attrib["label"]
            self.scheme = categoryNode.attrib["scheme"]
            self.term = categoryNode.attrib["term"]
        except AttributeError:
            self.label = ""
            self.scheme = ""
            self.term = ""
        
# end Category