from datetime import datetime, timedelta

from enum import Enum

from Author import Author
from Link import Link
from Category import Category

import lxml.etree as ET
import geojson
from geojson import Point, LineString, Polygon, MultiPolygon

class LanguageType( Enum ):
    EN = "en"
    FR = "fr"
# end LanguageType enum

class StatusEnum( Enum ):
    StatusActual = "Actual"
    StatusExercise = "Exercise"
    StatusSystem = "System"
    StatusTest = "Test"
    StatusDraft = "Draft"
# end StatusEnum

class CategegoryEnum( Enum ):
    CategoryOther = "Other"
    CategoryGeophysical = "Geo"
    CategoryMeteorological = "Met"
    CategorySafety = "Safety"
    CategorySecurity = "Security"
    CategoryRescue = "Rescue"
    CatergoryFire = "Fire"
    CatergoryHealth = "Health"
    CatergoryEnvironmental = "Env"
    CatergoryTransportation = "Transport"
    CatergoryInfrastructure = "Infra"
    CatergoryCBRNE = "CBRNE"
    
#end CategoryEnum

class SeverityEnum( Enum ):
    SeverityUnknown = "Unknown"
    SeverityMinor = "Minor"
    SeverityModerate = "Moderate"
    SeveritySevere = "Severe"
    SeverityExtreme = "Extreme"
# end SeverityEnum

class CertaintyEnum( Enum ):
    CertaintyUnknown = "Unknown"
    CertaintyObserved = "Observed"
    CertaintyLikely = "Likely"
    CertaintyPossible = "Possible"
    CertaintyUnlikely = "Unlikely"
# end CertaintyEnum

class ColourEnum( Enum ):
    ColourBlue = "Blue"
    ColourGray = "Gray"
    ColourPurple = "Purple"
    ColourYellow = "Yellow"
    ColourGreen = "Green"
    ColourBlack = "Black"
    ColourRed = "Red"
# end ColourEnum
    
class Entry( object ):

    def __init__( self, languages = [ LanguageType.EN ] ):

        # Text dictionaries...
        self._titles = {}
        self._contents = {}
        self._summaries = {}
        self._rights = {}
        self._languages = {}
        
        #  Class helper properties...
        self.setLanguages( languages )

        # Entry Properties...
        self.id = ""
        self.author = Author()
        # NOTE: Language specific items will be accessible via methods
        # i.e. title, content, summary

        self.published = datetime.utcnow()
        self.updated = datetime.utcnow()
        self.edited = datetime.utcnow()
        self.expires = datetime.utcnow() + timedelta( minutes = 60 )
        self.effective = datetime.utcnow()

        # MASAS Categories...
        self.categories = []
        self.certainties = []
        self.colour = None
        self.icon = "other"
        self.severities = []
        self.status = StatusEnum.StatusExercise

        # ATOM Categories not processed...
        self.atom_categories = []

        # Geometry...
        self.geometry = None

        # Links...
        self.editLink = Link()
        self.editMediaLink = Link()
        self.historyLink = Link()

        self.related = []
        self.links = []

        # NOTE: These are the attachments, a whole other task is set aside to implement this!
        self.enclosures = []

    # end __init__

    def setTitle( self, title, language = LanguageType.EN ):
        self._titles[language] = title
    # end setTitle

    def getTitle( self, language = LanguageType.EN ):
        try:
            return self._titles[language]
        except KeyError:
            return None
    # end getTitle

    def setContent( self, content, language = LanguageType.EN ):
        self._contents[language] = content
    # end setContent

    def getContent( self, language = LanguageType.EN ):
        try:
            return self._contents[language]
        except KeyError:
            return None
    # end getContent

    def setSummary( self, summary, language = LanguageType.EN ):
        self._summaries[language] = summary
    # end setSummary

    def getSummary( self, language = LanguageType.EN ):
        try:
            return self._summaries[language]
        except KeyError:
            return None
            
    # end getSummary

    def setRights( self, right, language = LanguageType.EN ):
        self._rights[language] = right
    # end setRights

    def getRights( self, language = LanguageType.EN ):
        try:
            return self._rights[language]
        except KeyError:
            return None
    # end getRights

    def setLanguages( self, languages = [ LanguageType.EN ] ):
        # Validate the languages:  we need at least one!
        if len( self._languages ) == 0:
            self._languages = [ LanguageType.EN ]
        # end if
    # end setLanguages

    def getLanguages( self ):
        return self._languages
    # end getLanguages

    def getLanguageType(self, attribute):
        Language = None
        if attribute.attrib["{http://www.w3.org/XML/1998/namespace}lang"] == "en":
            Language = LanguageType.EN
        elif attribute.attrib["{http://www.w3.org/XML/1998/namespace}lang"] == "fr":
            Language = LanguageType.FR
        return Language
    #end getLanguageType

    #Select and add correct categories based on XML category Element
    def setCategory(self, categoryElement):
        #If Certainty
        if categoryElement.label == "Certainty":
            if categoryElement.term == "Unknown":
                self.certainties.append(CertaintyEnum.CertaintyUnknown)
            elif categoryElement.term == "Observed":
                self.certainties.append(CertaintyEnum.CertaintyObserved)
            elif categoryElement.term == "Likely":
                self.certainties.append(CertaintyEnum.CertaintyLikely)
            elif categoryElement.term == "Possible":
                self.certainties.append(CertaintyEnum.CertaintyPossible)
            elif categoryElement.term == "Unlikely":
                self.certainties.append(CertaintyEnum.CertaintyUnlikely)
        #If Colour
        elif categoryElement.label == "Colour":
            if categoryElement.term == "Blue":
                self.status = ColourEnum.ColourBlue
            elif categoryElement.term == "Gray":
                self.status = ColourEnum.ColourGray
            elif categoryElement.term == "Purple":
                self.status = ColourEnum.ColourPurple
            elif categoryElement.term == "Yellow":
                self.status = ColourEnum.ColourYellow
            elif categoryElement.term == "Green":
                self.status = ColourEnum.ColourGreen
            elif categoryElement.term == "Black":
                self.status = ColourEnum.ColourBlack
            elif categoryElement.term == "Red":
                self.status = ColourEnum.ColourRed
        #If Icon
        elif categoryElement.label == "Icon":
            self.icon = categoryElement.term
        #If Severity
        elif categoryElement.label == "Severity":
            if categoryElement.term == "Unknown":
                self.severities.append(SeverityEnum.SeverityUnknown)
            elif categoryElement.term == "Minor":
                self.severities.append(SeverityEnum.SeverityMinor)
            elif categoryElement.term == "Moderate":
                self.severities.append(SeverityEnum.SeverityModerate)
            elif categoryElement.term == "Severe":
                self.severities.append(SeverityEnum.SeveritySevere)
            elif categoryElement.term == "Extreme":
                self.severities.append(SeverityEnum.SeverityExtreme)
        #If Status
        elif categoryElement.label == "Status":
            if categoryElement.term == "Actual":
                self.status = StatusEnum.StatusActual
            elif categoryElement.term == "Exercise":
                self.status = StatusEnum.StatusExercise
            elif categoryElement.term == "System":
                self.status = StatusEnum.StatusSystem
            elif categoryElement.term == "Test":
                self.status = StatusEnum.StatusTest
            elif categoryElement.term == "Draft":
                self.status = StatusEnum.StatusDraft
        #If Category
        elif categoryElement.label == "Category":
            if categoryElement.term == "Other":
                self.categories.append(CategegoryEnum.CategoryOther)
            elif categoryElement.term == "Geo":
                self.categories.append(CategegoryEnum.CategoryGeophysical)
            elif categoryElement.term == "Met":
                self.categories.append(CategegoryEnum.CategoryMeteorological)
            elif categoryElement.term == "Safety":
                self.categories.append(CategegoryEnum.CategorySafety)
            elif categoryElement.term == "Security":
                self.categories.append(CategegoryEnum.CategorySecurity)
            elif categoryElement.term == "Rescue":
                self.categories.append(CategegoryEnum.CategorySecurity)
            elif categoryElement.term == "Fire":
                self.categories.append(CategegoryEnum.CatergoryFire)
            elif categoryElement.term == "Health":
                self.categories.append(CategegoryEnum.CatergoryHealth)
            elif categoryElement.term == "Env":
                self.categories.append(CategegoryEnum.CatergoryEnvironmental)
            elif categoryElement.term == "Transport":
                self.categories.append(CategegoryEnum.CatergoryTransportation)
            elif categoryElement.term == "Infra":
                self.categories.append(CategegoryEnum.CatergoryInfrastructure)
            elif categoryElement.term == "CBRNE":
                self.categories.append(CategegoryEnum.CatergoryCBRNE)
          
        #end setCategory
                
    def toXML( self ):
        NMSPC = {None:'http://www.w3.org/2005/Atom', 'age':'http://purl.org/atompub/age/1.0', 'app':'http://www.w3.org/2007/app', 'met':'masas:experimental:time', 'georss':'http://www.georss.org/georss' }
        # build a tree structure
        entryXML = ET.Element('entry', nsmap = NMSPC)

        #Add the author
        author = self.author.toElementNode()
        entryXML.append(author)
        
        #Add the ID
        id = ET.SubElement(entryXML, "id")
        id.text = self.id
        
        #Add all categories to Entry

        # MASAS Category
        for category in self.categories:
            entryXML.append( Category( label="Category", scheme="masas:category:category", term=category ).toElementNode() )
        # end for

        # MASAS Certainty
        for certainty in self.certainties:
            entryXML.append( Category( label="Certainty", scheme="masas:category:certainty", term=certainty ).toElementNode() )
        # end for
 
        # MASAS Colour
        if( self.colour is not None ):
            entryXML.append( Category( label="Colour", scheme="masas:category:colour", term=self.colour ).toElementNode() )
        # end if
  
        # MASAS Icon
        if( self.icon is not None ):
            entryXML.append( Category( label="Icon", scheme="masas:category:icon", term=self.icon ).toElementNode() )
        # end if
 
        # MASAS Severity
        for severity in self.severities:
            entryXML.append( Category( label="Severity", scheme="masas:category:severity", term=severity ).toElementNode() )
        # end for
  
        # MASAS Status
        if( self.status is None ):
            raise ValueError( "Status is set to None; this is a required field." )
        # end if
  
        entryXML.append( Category( label="Status", scheme="masas:category:status", term=self.status ).toElementNode() )

        # ATOM Categories
        for category in self.atom_categories:
            if( isinstance( category, Category ) ):
                entryXML.append( category.toElementNode() )
            else:
                raise ValueError( "An Atom Category must be an instance of the class Category." )
            # end if
        # end for

        #Add all links to Entry
        for link in self.links:
            linkNode = link.toElementNode()
            entryXML.append(linkNode)
        NM = {'xml':'http://www.w3.org/1999/xhtml', None:'http://www.w3.org/1999/xhtml'}
        #Create title branches    
        title = ET.SubElement(entryXML, "title", type="xhtml")
        titlediv = ET.SubElement(title, "div", nsmap = NM)
        
        #Add titles for all languages
        for key, value in self._titles.iteritems():
            t = ET.Element("div", nsmap = NM)
            t.set('{http://www.w3.org/1999/xhtml}lang', key)
            t.text = value
            titlediv.append(t)
        
        #Create content Branches    
        content = ET.SubElement(entryXML, "content", type="xhtml")
        contentdiv = ET.SubElement(content, "div", nsmap = NM)
        
        #Add content for all languages
        for key, value in self._contents.iteritems():
            con = ET.Element("div")
            con.set('{http://www.w3.org/1999/xhtml}lang', key)
            con.text = value
            contentdiv.append(con)
            
        #Include the expired time    
        expiresXML = ET.SubElement(entryXML, "{http://purl.org/atompub/age/1.0}expires")
        expiresXML.text = self.expires.strftime("%Y-%m-%dT%H:%M:%SZ")
        
        #Add updated if required
        if self.updated:
            updatedXML = ET.SubElement(entryXML, "updated")
            updatedXML.text = self.updated.strftime("%Y-%m-%dT%H:%M:%SZ")
            
        #Add published if required
        if self.published:
            publishedXML = ET.SubElement(entryXML, "published")
            publishedXML.text = self.published.strftime("%Y-%m-%dT%H:%M:%SZ")
            
        #Add edited if required
        if self.edited:
            editedXML = ET.SubElement(entryXML, "{http://www.w3.org/2007/app}edited")
            editedXML.text = self.edited.strftime("%Y-%m-%dT%H:%M:%SZ")
            
        #Add effective if required
        if self.effective:
            effectiveXML = ET.SubElement(entryXML, "{masas:experimental:time}effective")
            effectiveXML.text = self.effective.strftime("%Y-%m-%dT%H:%M:%SZ")
            
        #Convert Geometry    
        if self.geometry.type == "Point":
            geometryXML = ET.SubElement(entryXML, "{http://www.georss.org/georss}point")
            pointString = str(self.geometry.coordinates[0]) + " " + str(self.geometry.coordinates[1])
            geometryXML.text = pointString
        if self.geometry.type == "LineString":
            geometryXML = ET.SubElement(entryXML, "{http://www.georss.org/georss}line")
            pointString = ""
            for points in self.geometry.coordinates:
                pointString += str(points[0]) + " " + str(points[1]) + " "
        if self.geometry.type == "Polygon":
            geometryXML = ET.SubElement(entryXML, "{http://www.georss.org/georss}polygon")
            pointString = ""
            for points in self.geometry.coordinates[0]:
                pointString += str(points[0]) + " " + str(points[1]) + " "
        if self.geometry.type == "MultiPolygon":
            geometryXML = ET.SubElement(entryXML, "{http://www.georss.org/georss}polygon")
            pointString = ""
            for points in self.geometry.coordinates[0]:
                pointString += str(points[0]) + " " + str(points[1]) + " "
            
        geometryXML.text = pointString
        # wrap it in an ElementTree instance, and save as XML
        tree = ET.ElementTree(entryXML)
        return ET.tostring(entryXML, pretty_print=True)
    
        # end toXML

    def setDataFromXML( self, xml ):
        root = ET.fromstring(xml)
        NMSPC = {'atom':'http://www.w3.org/2005/Atom', 'namespace':'http://www.w3.org/XML/1998/namespace', 'age':'http://purl.org/atompub/age/1.0', 'app':'http://www.w3.org/2007/app', 'met':'masas:experimental:time', 'georss':'http://www.georss.org/georss' }

        #Find author Branch
        authorXML = root.find('atom:author',  namespaces=NMSPC)
        self.author.fromElementNode(authorXML, NMSPC)
        self.id = root.findtext('atom:id', namespaces=NMSPC)

        #Find Categories
        categoryXML = root.findall('atom:category', namespaces=NMSPC)
        for category in categoryXML:
            cat = Category()
            cat.fromElementNode(category)
            self.setCategory(cat)
            
                    
        #Find Content Branch
        contentXML = root.find('atom:content', namespaces=NMSPC)

        contents = list(list(contentXML)[0]) #Return the branch that contains the contents
        for content in contents:
            Language = self.getLanguageType(content)
            self.setContent(content.text, Language)
                
        #Find Links
        linkXML = root.findall('atom:link', namespaces=NMSPC)
        for links in linkXML:
            link = Link()
            link.fromElementNode(links)
            if links.attrib["rel"] == "edit":
                self.editLink = link
            elif links.attrib["rel"] == "edit-media":
                self.editMediaLink = link
            elif links.attrib["rel"] == "history":
                self.historyLink = link
            self.links.append(link)
            
        #Find Title Branch
        titleXML = root.find('atom:title', namespaces=NMSPC)
        titles = list(list(titleXML)[0]) #Return the branch that contains the contents
        for title in titles:
            Language = self.getLanguageType(title)
            self.setTitle(content.text, Language)
                
        #Find Times
        publishedXML = root.findtext('atom:published', namespaces=NMSPC)
        updatedXML = root.findtext('atom:updated', namespaces=NMSPC)
        editedXML = root.findtext('app:edited', namespaces=NMSPC)
        expiresXML = root.findtext('age:expires', namespaces=NMSPC)
        effectiveXML = root.findtext('met:effective', namespaces=NMSPC)
        if publishedXML != None:
            try:
              self.published = datetime.strptime(publishedXML, "%Y-%m-%dT%H:%M:%SZ")
            except Exception:
              try:
                self.published = datetime.strptime(publishedXML, "%Y-%m-%dT%H:%M:%S.%fZ")
              except Exception:
                print("in exception self.edited ERROR. publishedXML = " + publishedXML)

        if updatedXML != None:
            print("updatedXML = " + updatedXML)
            try:
              self.updated = datetime.strptime(updatedXML, "%Y-%m-%dT%H:%M:%SZ")
            except Exception:
              try:
                print("updatedXML may have microseconds - attempting")
                self.updated = datetime.strptime(updatedXML, "%Y-%m-%dT%H:%M:%S.%fZ")
              except Exception:
                print("in exception self.updated ERROR. updatedXML = " + updatedXML)

        if editedXML != None:
            try:
              self.edited = datetime.strptime(editedXML, "%Y-%m-%dT%H:%M:%SZ")
            except Exception:
              try:
                self.edited = datetime.strptime(editedXML, "%Y-%m-%dT%H:%M:%S.%fZ")
                print("set editedXML from microseconds")
              except Exception:
                print("in exception self.edited ERROR. editedXML = " + editedXML)

        if expiresXML != None:
            try:
              self.expires = datetime.strptime(expiresXML, "%Y-%m-%dT%H:%M:%SZ")
            except Exception:
              try:
                self.expires = datetime.strptime(expiresXML, "%Y-%m-%dT%H:%M:%S.%fZ")
              except Exception:
                print("in exception self.edited ERROR. expiresXML = " + expiresXML)

        if effectiveXML != None:
            try:
              self.effective = datetime.strptime(effectiveXML, "%Y-%m-%dT%H:%M:%SZ")
            except Exception:
              try:
                self.effective = datetime.strptime(effectiveXML, "%Y-%m-%dT%H:%M:%S.%fZ")
              except Exception:
                print("in exception self.edited ERROR. effectiveXML = " + effectiveXML)
        
        #Find Geometry
        geometryPointXML = root.findtext('georss:point', namespaces=NMSPC)
        geometryPolygonXML = root.findtext('georss:polygon', namespaces=NMSPC)
        geometryLineXML = root.findtext('georss:line', namespaces=NMSPC)
        
        if geometryPointXML:
            geojsonString = '{"coordinates":['
            pointSplit = geometryPointXML.split()
            for p in pointSplit:
                geojsonString += p + ","
            geojsonString = geojsonString[:-1]
            geojsonString += '], "type": "Point"}'
            self.geometry = geojson.loads(geojsonString)
        elif geometryPolygonXML:
            pointSplit = geometryPolygonXML.split()
            geojsonString = '{"coordinates":[['
            n = 0
            for p in pointSplit:
                if n == 0:
                    geojsonString += str(p) + ","
                    n = n + 1
                else:
                    geojsonString += str(p) + "],["
                    n = 0
            geojsonString = geojsonString[:-2]
            geojsonString += '], "type": "Polygon"}'
            self.geometry = geojson.loads(geojsonString)
        elif geometryLineXML:
            pointSplit = geometryLineXML.split()
            geojsonString = '{"coordinates":[['
            n = 0
            for p in pointSplit:
                if n == 0:
                    geojsonString += str(p) + ","
                    n = n + 1
                else:
                    geojsonString += str(p) + "],["
                    n = 0
            geojsonString = geojsonString[:-2]
            geojsonString += '], "type": "LineString"}'
            self.geometry = geojson.loads(geojsonString)

        
    # end setDataFromXML

    @staticmethod
    def createFromXML( xml ):
        # Create an Entry using the given xmlData String
        entry = Entry()
        entry.setDataFromXML( xml )

        return entry
    # end CreateFromXML

# end Entry Class