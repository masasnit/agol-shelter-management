import lxml.etree as ET

class Link( object ):

    def __init__( self ):
        self.href = ""
        self.length = 0
        self.rel = ""
        self.title = ""
        self.type = ""
    # end __init__
    
    #Returns Link as an XML element
    def toElementNode(self):
        linkNode = ET.Element("link", href=self.href, rel=self.rel)
        if self.type:
            linkNode.attrib["type"] = self.type
        if self.title:
            linkNode.attrib["title"] = self.title
        return linkNode    
    # end toElementNode
    
    #Creates an Link from an XML element
    def fromElementNode(self, linkNode):
        try:
            self.href = linkNode.attrib.get("href")
            self.rel = linkNode.attrib.get("rel")
            self.type = linkNode.attrib.get("type")
            self.title = linkNode.attrib.get("title")
        except AttributeError:
            self.href = ""
            self.length = 0
            self.rel = ""
            self.title = ""
            self.type = ""

    # end fromElementNode
# end Link