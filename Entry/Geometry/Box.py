from src.client.Entry.Geometry import Geometry, Point

class Box( Geometry ):

    # Box Constructor
    def __init__( self, lowerLeft = Point(), upperRight = Point() ):
        self.lowerLeft = lowerLeft
        self.upperRight = upperRight
    # end __init__

    # toXML
    # Export the current object as an XML string
    def toXML( self ):
        raise NotImplementedError()
    # end toXML
    
    def setDataFromXML( self, xml ):
        raise NotImplementedError()
    # end setDataFromXML

    # createFromXML
    # Create a new geometry using an XML string.
    @staticmethod
    def createFromXML( xmlData ):
        raise NotImplementedError()
    # end createFromXML

# end Class Box