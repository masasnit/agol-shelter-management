from src.client.Entry.Geometry import Geometry

class Polyline( Geometry ):

    # Polyline Constructor
    def __init__( self, points = [] ):
        self.points = points
    # end __init__

    # toXML
    # Export the current object as an XML string
    def toXML( self ):
        raise NotImplementedError()
    # end toXML

    def setDataFromXML( self, xml ):
        raise NotImplementedError()
    # end setDataFromXML

    # createFromXML
    # Create a new geometry using an XML string.
    @staticmethod
    def createFromXML( xmlData ):
        raise NotImplementedError()
    # end createFromXML

# end Class Polyline