
class Geometry( object ):

    # Geometry Constructor
    def __init__( self ):
        raise NotImplementedError()
    # end __init__

    # toXML
    # Export the current object as an XML string
    #
    # The Geometry Class shouldn't return anything, a child Class
    # should implement this method as needed.
    def toXML( self ):

        raise NotImplementedError()
    # end toXML

# end Class Geometry