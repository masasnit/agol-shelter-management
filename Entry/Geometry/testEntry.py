def test(self):
    from src.client.Entry.Entry import Entry, LanguageType
    
    entry = Entry()
    entry.id = "myuniqueid"
    entry.setTitle( "An accident has occured!" )
    entry.setTitle( "Il y a eu un accident!", LanguageType.FR )
    entry.setContent( "Blah, blah, blah..." )
    entry.setContent( "Blah, blah, blah...", LanguageType.FR )
    
    masas_client = MASASClient( "https://...", "secret" )
    masas_client.add_entry( entry )