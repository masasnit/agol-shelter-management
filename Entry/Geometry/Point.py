from src.client.Entry.Geometry import Geometry

class Point( Geometry ):

    # Point Constructor
    # The latitude and longitude are defined as NaN by default to indicate
    # that no value has been set and the data is not valid.
    # Using 0.0, 0.0 is simply a bad way of doing this since they are valid
    # coordinates.
    def __init__( self, latitude = float('nan'), longitude = float('nan') ):
        self.latitude = latitude
        self.longitude = longitude
    # end __init__

    # toXML
    # Export the current object as an XML string
    def toXML( self ):
        # Note: Validate the numbers using math.isnan( x )
        raise NotImplementedError()
    # end toXML

    def setDataFromXML( self, xml ):
        raise NotImplementedError()
    # end setDataFromXML

    # createFromXML
    # Create a new geometry using an XML string.
    @staticmethod
    def createFromXML( xmlData ):
        raise NotImplementedError()
    # end createFromXML

# end Class Point