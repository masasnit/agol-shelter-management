# Shelter Management Tool #



# TO DO: #

* Hub Configuration - put HUB (Exercise OR Sandbox only for now) into the config.
* Enable / Disable EXERCISE mode (via config)
* Enable / Disalbe TWEETING (via config)
* User Mgmt - consider for access control and for
* ReadOnly View mode for general public
* Consider Public access (what does this do for edits/adds?)
* Review Input from CRC from past deployments
* Refactor to use Node.js



The Shelter Management tool is a basic web application that allows a community to manage information about the shelters in its area. Multiple agencies can collaborate via a shared data layer that is stored in ArcGIS OnLine (AGOL). Managers are able to send out Tweets (Chatter status updates for #CAUSE3 experiment).

Portions loosely based on https://gist.github.com/mccahill/6378276

## What does the tool do? ###

* Lists All Shelters - Provides a community-wide layer of shelter information:
    * on a map; and
    * in a list view.
* Allows a Manager to edit (add/update) Shelters:

* Allows a Shelter Manager to send out social media updates for a particular shelter. The system will create a pre-formatted, spatially tagged item.



### Contribution guidelines ###




* Writing tests
* Code review
* Other guidelines

## Key APIs In Use ##
This application is quite minimalist and stores most information in AGOL and uses Twitter/Chatter APIs for the social media integration.

### Twitter Integration ###
Tweets are sent using the [Twitter REST API statuses/update](https://dev.twitter.com/docs/api/1.1/post/statuses/update) 
The application only stores a link to Twitter using the Twitter OAuth token. No username or password information is stored.

### BitlyIntegration ###
Bitly is used to create short tweet-able URLs that can be stored and reused by the shelter manager.

Python Dependencies
* cherrypy
* pyproj
* geojson
* bitly_api
* tweepy

Other Dependencies
* Arcgis online account (https://www.arcgis.com/home/)
* bitly account (https://bitly.com)
* twitter account (https://twitter.com)
* MASAS account (https://www.masas-x.ca/en/)

Follow the instructions given by each site to get an account

## Configuring ##
Copy the above repository to your server and begin configuring the application. All config elements can be found in two places:
* config-debug.py or config-nodebug.py
* static/app_settings_shelter-template.js NOTE: Create new version called static/app_settings_shelter.js based on this file.

.gitignore will ignore *static/app_settings_shelter.js* and *config.py*

## Python Config ##
* server.socket_host: The location this application will run.
```
#!python

Ex: (0.0.0.0)
```

* server.socket_port: The port used by the application

```
#!python

Ex: 9999
```

* log.access_file: Name of the access file log

```
#!python

Ex: access.log
```

* log.error_file: Name of the error file log

```
#!python

Ex: error.log
```

* baseURL: The URL where the shelter manager will be deployed. Used internally to map the application back to itself.

```
#!python

Ex: www.someurl.com/shelter/
```

* LayerURL: The URL rest endpoint of the ArcGIS Online layer used by the shelter manager.

```
#!python

Ex: http://services1.arcgis.com/XXXX/ArcGIS/rest/services/Shelter/FeatureServer/2
```

* masasAccount: A list of ArcGIS Online usernames used by the shelter manager mapped to the MASAS access code of the user. Used when posting to MASAS. Add as many accounts here as required.

```
#!python

"agol_account" : "acbd1234"
```

* bitlyAccount: The bitly account created for the use with the shelter manager. Used to create shorter URLs stored in ArcGIS Online and tweeted out by admins. The login and token will be provided after creating a bitly account.

```
#!python

bitlyAccount  = { "login" : "account_name",
    "token" : "XXXXXXXXXXXXXXXXXXXXX"}
```

* twitterSettings: The twitter account created for the use with the shelter manager. A shelter manager Twitter application should be created to access the Twitter API (https://apps.twitter.com/app/new). After the twitter application is created the consumer key and consumer secret will be provided.

```
#!python

twitterSettings = {
    "consumer_key" : "XXXXXXXXXXX",
    "consumer_secret" :  "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"}
```


## app_settings_shelter.js ##
* clickCreate: Settings used by the app itself. Leave set to “false”.
* layerURL: The URL rest endpoint of the ArcGIS Online layer used by the shelter manager.

```
#!python

Ex: http://services1.arcgis.com/XXXX/ArcGIS/rest/services/Shelter/FeatureServer/2
```
* appId: The app ID of your ArcGIS Application. Found here: https://developers.arcgis.com/en/applications/ Click/create your application and find the "Client ID".

```
#!python

Ex: http://services1.arcgis.com/XXXX/ArcGIS/rest/services/Shelter/FeatureServer/2
```

* extentParams: The default view used by the shelter manager. The coordinates create a bounding box that is used to pan the view extent.

```
#!python

Ex: {"xmin": -65, "ymin": 43, "xmax": -60, "ymax":47 }
```

* extentZoomFactor: A factor for converting point to extent. Zooms the view in or out.

```
#!javascript

Ex: 1000
```

## Configure ArcGIS Online ##
Firstly you need to log into your ArcGIS Online account and go to "**My Content**".

There you will create a new feature layer (you can use one of the templates and we will just ignore the first layer it creates).

Proceed to the admin REST page (found @ /arcgis/rest/admin/services/) for your account.

Here you will want to "**Add to Definition**" and add the contents for "**shelter_schema.json**".
This will create the layer you will use for the shelter manager.

Be sure to turn on Editing by other editors - only allow them to update/delete their own items. ArcGIS will insert fields for you

Next proceed to *https://developers.arcgis.com* and log in. You will need to create an application for the shelter manager.

It's a simple as registering the name for your application. Here you will also find your "**Client ID**" that you will need for the "**app_settings_shelter.js**".

Under "**Authentication**" you will need to set a "**New Redirect URI**" which is the URL where you are accessing your application.

You can add multiple here for testing purposes. Also be sure to go to "**Settings**" and set your permissions to **public**.

## Running the App ##
Once the app is configured run it via the command line in python. It will run at the specified host and port number found in config.py.


```
#!python

Ex: python shelter.py
```


You can host this process in Apache or another web server of your choice.

Apache Example:

```
#!apache

    # setup the proxy
    <Location /shelter>
        Order allow,deny
        allow from all
        ProxyPass http://localhost:9999/
        ProxyPassReverse http://localhost:9999/
    </Location>
```